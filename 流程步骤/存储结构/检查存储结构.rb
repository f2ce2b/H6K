# 这里只是删除存储目录中多余的内容 缺失的目录及文件会在写入时创建

存储结构和运行结果保持一致 = lambda do |实际结构, 理想结构, 路径=[]|
  实际结构.each do |键,值|
    # 当定义目录为空({})不参与清除内部文件
    if !理想结构.key?(键) && 理想结构 != {}
      文件路径 = [路径, 键].join('/')
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 删除文件: #{文件路径}\n"
      FileUtils.rm_r(文件路径)
    # 值可以不同 但是必须都为字符串
    elsif (值 != 理想结构[键] && [值.is_a?(String), 理想结构[键].is_a?(String)].uniq.size == 2)
      # 哈希代表目录 字符串代表文件
      if 值.is_a?(Hash) && 理想结构[键].is_a?(String)
        断点调试(binding)
        FileUtils.rm_r([路径, 键].join('/'))
      # else
      #   断点调试(binding)
      end
    elsif (值 == 理想结构[键]) || ([值.is_a?(String), 理想结构[键].is_a?(String)].uniq.size == 1)
      next
    # 结构定义为文件夹但实际存储结构内存在文件
    elsif 值.is_a?(Hash) && 理想结构 == {}
      next
    elsif 值.is_a?(Hash)
      存储结构和运行结果保持一致.call(值, 理想结构[键], [路径, 键].flatten)
    else
      断点调试(binding)
    end
  end
end

case @运行参数['目标目录']
when /.*\/[0-9]{6}(.SZ|.SH|.BJ)$/
  require_relative '../../面向对象/股票/加载.rb'
  理想存储结构 = @股票.存储结构[1]
when /.*\/[0-9]{6}$/
  require_relative '../../面向对象/基金/加载.rb'
  理想存储结构 = @基金.理想存储结构[1]
else
  断点调试(binding)
end

实际存储结构 = @存储目录.存储结构[1]
存储结构和运行结果保持一致.call(实际存储结构, 理想存储结构, [@存储目录.目标目录])