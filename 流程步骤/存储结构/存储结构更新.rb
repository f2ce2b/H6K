# 根据理想存储结构构建缺失的文件及目录

def 层级迭代(哈希:, 键组: [])
  层级地址 = File.join(键组.flatten)
  Dir.mkdir(层级地址) unless File::directory?(层级地址)

  哈希.each do |键, 值|
    case 值
    when Hash
      层级迭代(哈希: 值, 键组: [键组, 键])
    when String
      文件路径 = File.join([层级地址, 键]) 
      next if File::file?(文件路径)
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 创建文件: #{文件路径}\n"
      File.open(文件路径, 'w'){|文件对象| 文件对象.write('')}
    end
  end
end

case @运行参数['目标目录']
when /.*\/[0-9]{6}(.SZ|.SH|.BJ)$/
  require_relative '../../面向对象/股票/加载.rb'
  理想存储结构 = @股票.存储结构[1]
when /.*\/[0-9]{6}$/
  require_relative '../../面向对象/基金/加载.rb'
  理想存储结构 = @基金.理想存储结构[1]
else
  断点调试(binding)
end

层级迭代(哈希: 理想存储结构, 键组: [J结果集.目标目录])

J结果集.内容['/'] = @存储目录.存储结构[1]
J结果集.更新