return if @运行参数['性能测试']

总分数 = FC.设备列表.map{|k,v| v['权重分数']}.sum
待分配任务 = Dir["#{@运行参数['目标目录']}/*"]
# 待分配任务 = 待分配任务[0...30]
任务总数 = 待分配任务.size

FC.设备列表.each do |操作目标, 详情|
  if FC.设备列表.keys.last == 操作目标
    详情['设备分配目录'] = 待分配任务
    next
  end

  分配任务数 = (任务总数*(详情['权重分数']/总分数.to_f)).round
  详情['设备分配目录'] = 待分配任务.slice!(0, (分配任务数 == 0 ? 1 : 分配任务数))
  断点调试(binding) if 详情['设备分配目录'].size == 0
end

FC.设备列表.each do |操作目标, 详情|
  详情['任务数量'] = 详情['设备分配目录'].size
end