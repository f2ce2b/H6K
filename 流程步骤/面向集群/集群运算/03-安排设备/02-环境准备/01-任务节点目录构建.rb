FC.设备列表.each do |操作目标, 详情|
  任务目录 = [FC.任务目录, 操作目标].join('/')
  详情['任务目录'] = [FC.挂载目录, FC.任务标识, 操作目标].join('/')

  断点调试(binding) if File.directory?(任务目录)
  FileUtils.mkdir_p(任务目录)

  详情["设备分配目录"].each_slice(1000) do |批次任务目录|
    # FileUtils.ln_s(批次任务目录.join(' '), 任务目录)
    `ln -s #{批次任务目录.join(' ')} #{任务目录}`
    断点调试(binding) unless $?.exitstatus == 0
  end
end