result = L链接.访问(关键字: '历史净值')
断点调试(binding) unless result[0]
解析结果 = {}

result[1].each do |返回结构|
  remove_var = 返回结构.gsub(/(var\s[a-zA-Z]{1,}\=)(.*)(;$)/, '\2')
  html_content = remove_var.gsub(/(\{\scontent:\")(.*)(\",records.*$)/, '\2')
  html_object = Nokogiri::HTML.parse(html_content)

  datas = [html_object.css('th').map{|children| children.text}]

  datas = datas + html_object.css('tr').map do |children|
    children.css('td').map do |td|
      td.text
    end 
  end 

  datas.delete_if{|array| array == []}

  datas[1..-1].each do |array|
    hash = {}
    array.each_with_index do |item, index|
      hash.merge!({datas[0][index] => item})
    end 

    hash = hash.map do |键,值|
      case 键
      when "单位净值", "累计净值"
        [键,值.to_f]
      when "日增长率"
        ["日增长率(%)", 值.gsub('%', '').to_f]
      else
        [键,值]
      end
    end.to_h

    年,月,日 = array[0].split('-')
    (解析结果[年]||={})[[月,日].join('-')] = hash.to_json
  end 
end 

J结果集.内容更新(层级: ['历史净值'], 更新值: 解析结果)
