result = L链接.访问(关键字: '基本信息')
断点调试(binding) unless result[0]
解析对象 = Nokogiri::HTML.parse(result[1])

解析结果 = {}
解析对象.xpath("//*[@class='detail']").css('tr').each do |tr|
  th_contents = tr.css('th').map(&:text)
  td_contents = tr.css('td').map(&:text)
  tmp = {}
  th_contents.each_with_index do |th_content, index|
    tmp.merge!({th_content => td_contents[index]})
  end 
  解析结果.merge!(tmp)
end

# L链接.访问(关键字: '基本信息', 删除缓存: true) 
断点调试(binding) unless 解析结果['成立日期/规模'] =~ /\d{4}年\d{2}月\d{2}日\s\/\s[\d .]+亿份/

资产规模正则 = /([\d .]+)亿元（(截止至：\d{4}年\d{2}月\d{2}日)）/
断点调试(binding) unless 解析结果['资产规模'] =~ 资产规模正则

份额规模正则 = /([\d .]+)亿份（(截止至：\d{4}年\d{2}月\d{2}日)）/
断点调试(binding) unless 解析结果['份额规模'] =~ 份额规模正则

成立来分红正则 = /每份累计([\d .]+)元（(\d{1,})次）/
断点调试(binding) unless 解析结果['成立来分红'] =~ 成立来分红正则

管理费率正则 = /([\d .]+)%（每年）/
断点调试(binding) unless 解析结果['管理费率'] =~ 管理费率正则

托管费率正则 = /([\d .]+)%（每年）/
断点调试(binding) unless 解析结果['托管费率'] =~ 托管费率正则

最高认购费率正则 = /([\d .]+)%/
断点调试(binding) unless 解析结果['最高认购费率'] =~ 最高认购费率正则

格式化结果 = {
  '发行日期' => lambda{|value, *args| value.gsub(/年|月|日/, '-').split('-').join('-')},
  '成立日期' => lambda{|value, *args| args[0]['成立日期/规模'].split(' / ')[0].gsub(/年|月|日/, '-').split('-').join('-')},
  '成立规模(亿份)' => lambda{|value, *args| args[0]['成立日期/规模'].split(' / ')[1].gsub('亿份', '').to_f},
  '成立日期/规模' => lambda{|value, *args| nil},

  '资产规模(亿元)' => lambda{|value, *args| args[0]['资产规模'].gsub(资产规模正则, '\1').to_f},
  '资产规模说明' => lambda{|value, *args| args[0]['资产规模'].gsub(资产规模正则, '\2')},
  '资产规模' => lambda{|value, *args| nil},

  '份额规模(亿元)' => lambda{|value, *args| args[0]['份额规模'].gsub(份额规模正则, '\1').to_f},
  '份额规模说明' => lambda{|value, *args| args[0]['份额规模'].gsub(份额规模正则, '\2')},
  '份额规模' => lambda{|value, *args| nil},

  '每份累计分红(元)' => lambda{|value, *args| args[0]['成立来分红'].gsub(成立来分红正则, '\1').to_f},
  '分红次数' => lambda{|value, *args| args[0]['成立来分红'].gsub(成立来分红正则, '\2').to_i},
  '成立来分红' => lambda{|value, *args| nil},

  '管理费率(每年／%)' => lambda{|value, *args| args[0]['管理费率'].gsub(管理费率正则, '\1').to_f},
  '管理费率' => lambda{|value, *args| nil},

  '托管费率(每年／%)' => lambda{|value, *args| args[0]['托管费率'].gsub(托管费率正则, '\1').to_f},
  '托管费率' => lambda{|value, *args| nil},

  '销售服务费率(每年／%)' => lambda do |value, *args| 
    if args[0]['销售服务费率'] =~ /^[\d .]+%（每年）$/
      args[0]['销售服务费率'].gsub(/^([\d .]+)%（每年）$/, '\1').to_f
    elsif args[0]['销售服务费率'] == "---（每年）"
      0.0
    else
      断点调试(binding)
    end
  end,
  '销售服务费率' => lambda{|value, *args| nil},

  '最高认购费率(%)' => lambda{|value, *args| args[0]['最高认购费率'].gsub(最高认购费率正则, '\1').to_f},
  '最高认购费率' => lambda{|value, *args| nil},

  '业绩比较基准' => lambda{|value, *args| value.split('+')},
  
  '最高申购费率(%)' => lambda do |value, *args| 
    if args[0]['最高申购费率'] == '---'
      0.0
    elsif args[0]['最高申购费率'] =~ /([\d .]+)%（前端）/
      args[0]['最高申购费率'].gsub(/([\d .]+)%（前端）/, '\1').to_f
    else
      断点调试(binding)
    end
  end,
  '最高申购费率' => lambda{|value, *args| nil},

  '最高赎回费率(%)' => lambda do |value, *args| 
    if args[0]['最高赎回费率'] == '---'
      0.0
    elsif args[0]['最高赎回费率'] =~ /([\d .]+)%（前端）/
      args[0]['最高赎回费率'].gsub(/([\d .]+)%（前端）/, '\1').to_f
    elsif args[0]['最高赎回费率'] =~ /([\d .]+)%/
      args[0]['最高赎回费率'].gsub(/([\d .]+)%/, '\1').to_f
    else
      断点调试(binding)
    end
  end,
  '最高赎回费率' => lambda{|value, *args| nil},
}

格式化结果.each do |键,值|
  解析结果[键] = 值.call(解析结果[键], 解析结果)
end

解析结果.delete_if{|k,v| v.nil?}

res = @基金.转存储结构(层级: ['基本概况'], 对象: 解析结果)
断点调试(binding) unless res[0]
J结果集.内容更新(层级: ['基本概况'], 更新值: res[1])