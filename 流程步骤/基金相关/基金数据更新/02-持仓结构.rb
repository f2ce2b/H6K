result = L链接.访问(关键字: '持仓结构')
断点调试(binding) unless result[0]
解析结果 = {}

# TODO: 这种情况一般出现在上方已经存在相关股票行 
# 但上下两行的占比以及持股数等数据不同 具体原因目前未知
def 特殊数据行解析(数据行:, 解析列:)
  定义格式 = {
    "序号" => /^\d{1,}$/,
    "股票代码" => /^\d{5,6}$/, # 2010年11月22日后注册的港股是6位之前的还是5位代码
    "股票名称" => /^[\u4e00-\u9fa5 A-Z \*]+$/,
    "相关资讯" => /^股吧行情$/,
    "占净值比例" => /^[\d \.]+%$/,
    "持股数（万股）" => /^[\d \.]+$/,
    "持仓市值（万元）" => /^[\d \. \,]+$/,
  }
  
  定义格式.each_with_index do |数组, 下标|
    断点调试(binding) unless 数据行.at(下标) =~ 数组[1]
  end

  indexs = 解析列.map{|item|定义格式.keys.index(item)}
  断点调试(binding) if indexs.include?(nil)

  indexs.map{|index| 数据行.at(index).gsub(',', '')}
end

def 持仓结构_返回结构解析(table_conent:)
  表格列 = table_conent.css('thead')[0].css('tr')[0].css('th').map{|children| children.text}
  解析列 = ["股票代码", "股票名称", "占净值比例", "持股数（万股）", "持仓市值（万元）"]
  解析列下标 = 解析列.map{|item|表格列.index(item)}

  tmp = {}
  table_conent.css('tbody')[0].css('tr').each do |tr_content|
    数据行 = tr_content.css('td').map{|children| children.text}

    # 数据列差异问题: 部分表格内多出 最新价,涨跌幅两列,但在下面几行中缺又不存在这两列
    if 表格列.size == 数据行.size
      contents = 解析列下标.map{|index| 数据行.at(index).gsub(',', '')}
    else
      contents = 特殊数据行解析(数据行: 数据行, 解析列: 解析列)
    end

    tmp_ = {}
    解析列.each_with_index do |title, index|
      case title
      when /持股数（万股）/
        tmp_['持股数'] = {
          "数值" => contents[index].to_f,
          "单位" => '万股'
        }
      when /持仓市值（万元）/
        tmp_['持仓市值'] = {
          "数值" => contents[index].to_f,
          "单位" => '万元'
        }
      else
        tmp_.merge!(title => contents[index])
      end 
    end 

    键 = contents[1]
    if (tmp[键])
      count = tmp.keys.select{|value| value.include?(键)}.size
      tmp["#{键}-#{count+1}"] = tmp_
    else
      tmp[键] = tmp_
    end
    
  end 

  [true, tmp]
end

result[1].each do |返回结构|
  html_content = 返回结构.gsub(/var apidata={ content:\"(.*)(\",arryear.*$)/, '\1')

  if html_content == ''
    next if 返回结构.include?('arryear:[]')

    存在年份 = (JSON.parse(返回结构.gsub(/.*arryear:(\[.*\]).*/, '\1')) rescue 断点调试(binding))
    当前年份 = (返回结构.gsub(/.*curyear:(\d{4}).*/, '\1').to_i rescue 断点调试(binding))
    断点调试(binding) unless 当前年份 != 0
    next unless 存在年份.include?(当前年份)

    断点调试(binding)
  end
  html_object = Nokogiri::HTML.parse(html_content)
  
  html_object.css('table').each_with_index do |table, index|
    h4 = html_object.css('h4')[index]
    key = h4.css('label')[-1].text.gsub(/.*(\d{4}\-\d{2}\-\d{2})/, '\1')
    res = 持仓结构_返回结构解析(table_conent: table)
    解析结果[key] = {
      "标题" => h4.css('label')[0].text.gsub('  ', '-'),
      "构成" => res[1]
    }.to_json
  end

end

J结果集.内容更新(层级: ['持仓结构'], 更新值: 解析结果)