require_relative '../../面向对象/Ruby/Ruby.rb'

def 层级迭代(哈希:, 键组: [])
  层级地址 = File.join(键组.flatten)
  Dir.mkdir(层级地址) unless File::directory?(层级地址)

  哈希.each do |键, 值|
    case 值
    when Hash
      层级迭代(哈希: 值, 键组: [键组, 键])
    when String, NilClass, Float, Integer
      文件路径 = File.join([层级地址, 键.to_s]) 
      文件已存在 = false
      if File::file?(文件路径)
        文件已存在 = true
        md5_值 = Digest::MD5.hexdigest((JSON.pretty_generate(JSON.parse(值)) rescue 值).to_s)
        文件内容 = File.open(文件路径){|file|file.read}
        md5_文件 = Digest::MD5.hexdigest(文件内容)
        next if md5_值 == md5_文件
        # 非特殊情况不更新现有文件 但似乎不太容易实现
        # 断点调试(binding) unless [nil, ''].include?(文件内容)
      end 

      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] #{文件已存在 ? '更新文件' : '新建文件'}: #{文件路径}\n"
      File.open(文件路径, 'w') do |文件对象|
        begin
          文件对象.write(JSON.pretty_generate(JSON.parse(值)))  
        rescue => exception
          文件对象.write(值)
        end
      end
    else
      断点调试(binding)
    end
  end
end

层级迭代(哈希: J结果集.内容['/'], 键组: [J结果集.目标目录])