# # 初次运行太慢
# 相关文件 = Dir["#{@存储目录.目标目录}/**/异常属性值统计.json"]
相关文件 = Dir["#{@存储目录.目标目录}/*"].map do |dir| 
  Dir["#{dir}/*/异常属性值统计.json"]
end.flatten

异常属性值 = {}

相关文件.each do |文件|
  print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 开始解析: #{文件}\n"
  JSON.parse(File.open(文件){|f|f.read}).each do |键,值|
    值['存储路径'] = 文件
    (异常属性值[键]||=[]) << 值
  end
end

@数据['异常属性值'] = 异常属性值