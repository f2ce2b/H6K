全部索引 = Es.查看_索引
子目录名称 = Dir["#{@存储目录.目标目录}/*"].select{|df| File::directory?(df)}.map{|dir| dir.gsub("#{@存储目录.目标目录}/", '')}

匹配正则 = Regexp.new(子目录名称.join('|'))
相关索引 = 全部索引.select{|index| index['名称'] =~ 匹配正则 }

@数据['ElasticSearch/索引列表'] = 相关索引