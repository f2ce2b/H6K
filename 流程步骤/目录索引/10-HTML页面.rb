# @存储目录.存储结构

相关文件 = Dir["#{@存储目录.目标目录}/*"].map do |dir| 
  Dir["#{dir}/*/*.html"]
end.flatten

相关文件.each do |路径|
  分类名 = 路径.split('/')[-1].split('.')[0]
  关键词路径 = [路径.split('/')[0...-1], '关键词'].flatten.join('/')

  (@数据[分类名]||=[]) << {
    链接: {链接: 路径.gsub(@存储目录.目标目录+'/', '')}.to_json,
    关键词: (File.file?(关键词路径) ? File.open(关键词路径){|f| f.read.split("\n")}.to_json : '[]'),
  }
end