require_relative '../../../面向对象/股票/加载.rb'

result = L链接.访问(关键字: '上市公司概况')
断点调试(binding) unless result[0]
断点调试(binding) if result[1].values.flatten.size != 2

jbzl = {
  'SECUCODE' => 'A股代码',
  'SECURITY_CODE' => 'next',
  'SECURITY_NAME_ABBR' => 'A股简称',
  'ORG_CODE' => 'next', # 暂不确定具体含义
  'ORG_NAME' => '公司名称',
  'ORG_NAME_EN' => '英文名称',
  'FORMERNAME' => '曾用名',
  'STR_CODEA' => 'next',
  'STR_NAMEA' => 'next',
  'STR_CODEB' => 'B股代码',
  'STR_NAMEB' => 'B股简称',
  'STR_CODEH' => 'H股代码',
  'STR_NAMEH' => 'H股简称',
  'SECURITY_TYPE' => '证券类别',
  'EM2016' => '所属东财行业',
  'TRADE_MARKET' => '上市交易所',
  'INDUSTRYCSRC1' => '所属证监会行业',
  'PRESIDENT' => '总经理',
  'LEGAL_PERSON' => '法人代表',
  'SECRETARY' => '董秘',
  'CHAIRMAN' => '董事长',
  'SECPRESENT' => '证券事务代表',
  'INDEDIRECTORS' => '独立董事',
  'ORG_TEL' => '联系电话',
  'ORG_EMAIL' => '电子信箱',
  'ORG_FAX' => '传真',
  'ORG_WEB' => '公司网址',
  'ADDRESS' => '办公地址',
  'REG_ADDRESS' => '注册地址',
  'PROVINCE' => '区域',
  'ADDRESS_POSTCODE' => '邮政编码',
  'REG_CAPITAL' => '注册资本(万元)',
  'REG_NUM' => '工商登记',
  'EMP_NUM' => '雇员人数',
  'TATOLNUMBER' => '管理人员人数',
  'LAW_FIRM' => '律师事务所',
  'ACCOUNTFIRM_NAME' => '会计师事务所',
  'ORG_PROFILE' => '公司简介',
  'BUSINESS_SCOPE' => '经营范围',
  'EXPAND_NAME_ABBR' => 'A股扩位简称',

}

fxxg = {
  'SECUCODE' => 'next',
  'SECURITY_CODE' => 'next',
  'FOUND_DATE' => '成立日期',
  'LISTING_DATE' => '上市日期',
  'AFTER_ISSUE_PE' => '发行市盈率(倍)',
  'ONLINE_ISSUE_DATE' => '网上发行日期',
  'ISSUE_WAY' => '发行方式',
  'PAR_VALUE' => '每股面值(元)',
  'TOTAL_ISSUE_NUM' => '发行量(股)',
  'ISSUE_PRICE' => '每股发行价(元)',
  'DEC_SUMISSUEFEE' => '发行费用(元)',
  'TOTAL_FUNDS' => '发行总市值(元)',
  'NET_RAISE_FUNDS' => '募集资金净额(元)',
  'OPEN_PRICE' => '首日开盘价(元)',
  'CLOSE_PRICE' => '首日收盘价(元)',
  'TURNOVERRATE' => '首日换手率',
  'HIGH_PRICE' => '首日最高价(元)',
  'OFFLINE_VAP_RATIO' => '网下配售中签率',
  'ONLINE_ISSUE_LWR' => '定价中签率'
}

基本资料 = {}
result[1]['jbzl'][0].each do |键,值|
  断点调试(binding) unless jbzl.key?(键)
  next if jbzl[键] == 'next'
  基本资料[jbzl[键]] = case 键
  when 'ORG_TEL', 'INDEDIRECTORS', 'ADDRESS', 'ADDRESS_POSTCODE'
    (值||'').split(',')
  when 'EM2016', 'INDUSTRYCSRC1'
    (值||'').split('-')
  when 'ORG_WEB'
    值 || '暂无'
  when 'EMP_NUM'
    值 || 0
  when 'ORG_PROFILE'
    (值||'').gsub(/^\s{1,}(.*)/, '\1').gsub("\n", '').split('。')
  when 'BUSINESS_SCOPE'
    (值||'').split(';')
  when 'TRADE_MARKET'
    if 值
      值
    else
      case D东方财富.代码
      when /\.BJ$/
        '北京证券交易所'
      end
    end
  else
    值
  end
end

res = @股票.转存储结构(层级: ['公司概况', '基本资料'], 对象: 基本资料)
断点调试(binding) unless res[0]
基本资料 = res[1]
J结果集.内容更新(层级: ['公司概况', '基本资料'], 更新值: 基本资料)

发行相关 = {}
result[1]['fxxg'][0].each do |键,值|
  断点调试(binding) unless fxxg.key?(键)
  next if fxxg[键] == 'next'
  发行相关[fxxg[键]] = 值
end

res = @股票.转存储结构(层级: ['公司概况', '发行相关'], 对象: 发行相关)
断点调试(binding) unless res[0]
发行相关 = res[1]
J结果集.内容更新(层级: ['公司概况', '发行相关'], 更新值: 发行相关)