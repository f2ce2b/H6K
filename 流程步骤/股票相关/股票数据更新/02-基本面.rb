# https://emweb.securities.eastmoney.com/PC_HSF10/OperationsRequired/Index?type=web&code=sh600519

require_relative '../../../面向对象/股票/加载.rb'

result = L链接.访问(关键字: '基本面数据')
断点调试(binding) unless result[0]

if result[1] == ''
  result = L链接.访问(关键字: '基本面数据', 删除缓存: true) 
end

断点调试(binding) unless result[1]['zxzb'].size == 1

遍历寻值 = lambda do |键组:, 结果:|
  键组 = 键组.split('.') if 键组.is_a?(String)
  结果 = JSON.parse(结果, object_class: OpenStruct) if 结果.is_a?(String)
  断点调试(binding) unless 结果.respond_to?(键组[0])
  结果 = 结果.send(键组.delete_at(0))
  return 结果 if 键组 == []
  遍历寻值.call(键组: 键组, 结果: 结果)
end

对象翻译 = lambda do |对象:, 字典:, 结果:{}|
  case 字典
  when Hash
    字典.each do |键, 值|
      结果[键.to_s] = 对象翻译.call(对象: 对象, 字典: 值, 结果:(值.is_a?(Hash) ? 结果[键.to_s]={} : 结果[键.to_s]=nil))
    end
  when String
    结果 = 遍历寻值.call(键组: 字典, 结果: 对象)
  else
    断点调试(binding)
  end
  结果
end

字典_指标最新数据 = {
  '数据来源': 'zxzb.first.REPORT_TYPE',
  '基本每股收益(元)': 'zxzb.first.EPSJB',
  '每股净资产(元)': 'zxzb.first.BPS',
  '每股经营现金流(元)': 'zxzb.first.MGJYXJJE',
  '扣非每股收益(元)': 'zxzb.first.EPSKCJB',
  '每股公积金(元)': 'zxzb.first.MGZBGJ',
  '市盈率动(倍)': 'zxzbOther.first.PE_DYNAMIC', # 无法找到与页面完全相符的值
  '稀释每股收益(元)': 'zxzb.first.EPSXS',# 考察
  '每股未分配利润(元)': 'zxzb.first.MGWFPLR',
  '市盈率TTM(倍)': 'zxzbOther.first.PE_TTM', # 无法找到与页面完全相符的值
  '总股本(股)': 'zxzb.first.TOTAL_SHARE',
  '流通股本(股)': 'zxzb.first.FREE_SHARE',
  '市盈率静(倍)': 'zxzbOther.first.PE_STATIC', # 无法找到与页面完全相符的值
  '总市值(元)': 'zxzbhq.f116',# 考察
  '流通市值(元)': 'zxzbhq.f117',# 考察
  '市净率(倍)': 'zxzbhq.f167',
}

指标最新数据 = 对象翻译.call(对象: result[1].to_json, 字典: 字典_指标最新数据)

# 确认两个考察数据的具体值
断点调试(binding) unless 指标最新数据['总市值(元)'].to_f >= 指标最新数据['流通市值(元)'].to_f

断点调试(binding) unless [
  遍历寻值.call(键组: 'zxzb.first.EPSXS', 结果: result[1].to_json),
  遍历寻值.call(键组: 'zyzb.first.EPSXS', 结果: result[1].to_json),
].uniq.size == 1

断点调试(binding) unless [
  遍历寻值.call(键组: 'zxzb.first.EPSKCJB', 结果: result[1].to_json),
  遍历寻值.call(键组: 'zyzb.first.EPSKCJB', 结果: result[1].to_json),
].uniq.size == 1

层级 = ['基本面', '指标最新数据']
res = @股票.转存储结构(层级: 层级, 对象: 指标最新数据)
断点调试(binding) unless res[0]
J结果集.内容更新(层级: 层级, 更新值: res[1])

字典_指标对比数据 = {
  '加权净资产收益率(%)':{
    '最新数据':'zxzb.first.ROEJQ',
    '上年同期':'zxzb.first.ROEJQ_LAST',
  },
  '毛利率(%)':{
    '最新数据':'zxzb.first.XSMLL',
    '上年同期':'zxzb.first.XSMLL_LAST',
  },
  '资产负债率(%)':{
    '最新数据':'zxzb.first.ZCFZL',
    '上年同期':'zxzb.first.ZCFZL_LAST',
  },
  '营业总收入(元)':{
    '最新数据':'zxzb.first.TOTAL_OPERATEINCOME',
    '上年同期':'zxzb.first.TOTAL_OPERATEINCOME_LAST',
  },
  '营业总收入滚动环比增长(%)':{
    '最新数据':'zxzb.first.YYZSRGDHBZC',
    '上年同期':'zxzb.first.YYZSRGDHBZC_LAST',
  },
  '营业总收入同比增长(%)':{
    '最新数据':'zxzb.first.TOTALOPERATEREVETZ',
    '上年同期':'zxzb.first.TOTALOPERATEREVETZ_LAST',
  },
  '归属净利润(元)':{
    '最新数据':'zxzb.first.PARENT_NETPROFIT',
    '上年同期':'zxzb.first.PARENT_NETPROFIT_LAST',
  },
  '归属净利润滚动环比增长(%)':{
    '最新数据':'zxzb.first.NETPROFITRPHBZC',
    '上年同期':'zxzb.first.NETPROFITRPHBZC_LAST',
  },
  '归属净利润同比增长(%)':{
    '最新数据':'zxzb.first.PARENTNETPROFITTZ',
    '上年同期':'zxzb.first.PARENTNETPROFITTZ_LAST',
  },
  '扣非净利润(元)':{
    '最新数据':'zxzb.first.KCFJCXSYJLR',
    '上年同期':'zxzb.first.KCFJCXSYJLR_LAST',
  },
  '扣非净利润滚动环比增长(%)':{
    '最新数据':'zxzb.first.KFJLRGDHBZC',
    '上年同期':'zxzb.first.KFJLRGDHBZC_LAST',
  },
  '扣非净利润同比增长(%)':{
    '最新数据':'zxzb.first.KCFJCXSYJLRTZ',
    '上年同期':'zxzb.first.KCFJCXSYJLRTZ_LAST',
  },  
}

指标对比数据 = 对象翻译.call(对象: result[1].to_json, 字典: 字典_指标对比数据)

层级 = ['基本面', '指标对比数据']
res = @股票.转存储结构(层级: 层级, 对象: 指标对比数据)
断点调试(binding) unless res[0]
J结果集.内容更新(层级: 层级, 更新值: res[1])