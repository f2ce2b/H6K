FileUtils.rm_rf Dir.glob("#{@运行参数['目标目录']}/*")

@关键词组.each do |关键词, 数组|
  关键词路径 = [@运行参数['目标目录'], 关键词].join('/')
  FileUtils.mkdir_p(关键词路径)

  数组.each do |dir|
    断点调试(binding) unless File.file?("#{dir}/别名")
    别名 = File.open("#{dir}/别名"){|f| f.read}
    FileUtils.ln_s(dir, "#{关键词路径}/#{别名}")
  end
end