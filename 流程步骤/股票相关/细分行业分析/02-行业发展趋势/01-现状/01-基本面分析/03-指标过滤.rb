存储域 = J结果集.存储域(File.expand_path(__FILE__), 起始词: '行业分析')
各项指标均高于行业统计值 = @指标排序结果.clone

@指标_字段.each do |键,值|
  统计指标 = JSON.parse(J结果集.find_by(键: ['指标分析', 键].join('/')))['精简结构']
  ['中位数'].each do |指标|
    排序后 = @指标排序结果.select{|i| i[键] > 统计指标[指标]}.sort{ |x,y| y[键] <=> x[键] }
    存储域[[键, '大于', 指标].join] = 排序后.map{|i| [i['A股简称'], i[键]]}.to_h.to_json

    各项指标均高于行业统计值 = 各项指标均高于行业统计值.select{|i| i[键] > 统计指标[指标]}.sort{ |x,y| y[键] <=> x[键] }
  end
end

res = @权重排序.call(数据: 各项指标均高于行业统计值, 权重: @指标_权重)

龙头企业 = []
结果 = J结果集.扁平化结果.select{|k,v| k.include?('基本面分析/指标分析')}.clone
res[1]['数据'].each do |企业|
  处理后 = {}
  企业.each do |键,值|
    unless @基本面指标.keys.include?(键)
      处理后[键] = 值
      next
    end

    筛选结果 = 结果.select{|k,v| k =~ Regexp.new(".*#{键}$") }
    断点调试(binding) unless 筛选结果.keys.size == 1
    排名 = 筛选结果.values[0]['样本数据'].sort{|x, y| y <=> x}.index(企业[键])+1
    处理后["#{键}(排名)"] = "#{企业[键]}  (#{排名})"
  end
  龙头企业 << 处理后
end

存储域['各项指标均高于行业统计值'] = 龙头企业.to_json

龙头企业.each do |企业|
  代码 = JSON.parse(企业['A股代码'])['显示']
  企业['A股简称']
  关键词存储路径 = "#{@存储目录.目标目录}/#{代码}/关键词"
  断点调试(binding) unless File.file?(关键词存储路径)
  File.open(关键词存储路径, 'a'){|f| f.write("\n龙头企业")}
end