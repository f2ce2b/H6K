存储域 = J结果集.存储域(File.expand_path(__FILE__), 起始词: '行业分析')

指标列表字段 = {
  "A股代码" => ["*/**/*A股代码"],
  "A股简称" => ["*/**/*A股简称"],
}.merge!(@指标_字段)

@指标_权重 = @基本面指标.map{|k,v| [k, v['权重']]}.delete_if{|a| a.include?(nil)}.to_h

指标列表 = []

指标列表字段.each do |键,值|
  res = @存储目录.搜索(关键词: 值)
  断点调试(binding) unless res[0]
  res[1]['结果'].each_with_index do |value, index|
    (指标列表[index]||={})[键.to_s] = value
  end
end

指标列表.each do |行|
  行['A股代码'] = {显示: 行['A股代码'], 链接: [行['A股代码'], '个股信息.html'].join('/')}.to_json
end

@权重排序 = lambda do |数据:, 权重:|
  总分数 = 权重.values.sum
  权重 = 权重.map{|k,v| [k.to_s, sprintf('%.4f', (v.to_f / 总分数)).to_f]}.to_h
  排序结果 = {}
  数据.each do |行|
    权重分数 = 权重.map{|k,v| 行[k]*v}.sum
    排序结果[权重分数] = 行
  end
  
  [true, {
    '数据' => 排序结果.sort { |x,y| y <=> x }.map(&:last),
    '权重' => 权重
  }]
end

res = @权重排序.call(数据: 指标列表, 权重: @指标_权重)

存储域['指标列表'] = res[1]['数据'].to_json
存储域['指标列表排序权重'] = res[1]['权重'].to_json

@指标排序结果 = res[1]['数据']

@指标_字段.each do |键,值|
  存储域[键] = @指标排序结果.map{|i| [i['A股简称'], i[键]]}.to_h.to_json
end
