基本面指标 = {
  # 键名要和文件名做区分不然会被搜索到
  营业总收入: {
    关键词: ["*/**/*营业总收入(元)", "最新数据"],
    权重: 80
  },
  营业收入同比增长率: {
    关键词: ["*/**/*营业总收入同比增长*", "最新数据"],
    权重: 50,
  },
  归属净利润: {
    关键词: ["*/**/*归属净利润(元)", "最新数据"],
    权重: 80,
  },
  归属净利润同比增长: {
    关键词: ["*/**/*归属净利润同比增长(%)", "最新数据"],
    权重: 50,
  },
  每股净资产: {
    关键词: ["*/**/*每股净资产(元)"],
    权重: 80,
  },
}

@基本面指标 = JSON.parse(基本面指标.to_json)