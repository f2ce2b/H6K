require_relative '../../../../面向对象/股票/加载.rb'

res = J结果集.get(层级: ['公司概况', '基本资料'])
断点调试(binding) unless res[0]
基本资料 = res[1]

基本资料.each do |键,值|
  case 键
  when '所属东财行业', '所属证监会行业'
    基本资料[键] = 值.join('/')
  when '公司简介'
    基本资料[键] = 值#.join(".")
  end
end

字段定义 = {
  "公司名称" => {
    "类型": "keyword",
  },
 "A股代码" => {
    "类型": "keyword",
  },
 "A股简称" => {
    "类型": "keyword",
  },
 "证券类别" => {
    "类型": "keyword",
  },
 "所属东财行业" => {
    "类型": "keyword",
  },
 "上市交易所" => {
    "类型": "keyword",
  },
 "所属证监会行业" => {
    "类型": "keyword",
  },
 "公司网址" => {
    "类型": "text"
  },
 "区域" => {
    "类型": "keyword",
  },
 "邮政编码" => {
    "类型": "keyword",
  },
 "注册资本(万元)" => {
    "类型": "double"
  },
 "雇员人数" => {
    "类型": "integer"
  },
 "管理人员人数" => {
    "类型": "integer"
  },
 "公司简介" => {
    "类型": "text"
  },
 "经营范围" => {
    "类型": "text"
  }
}

定义及文档 = Es.生成索引定义及文档(定义: 字段定义, 目标对象: {基本资料['A股代码'] => 基本资料}, 说明: '股票_基本信息')

J结果集.内容更新(层级: ["ElasticSearch", "股票_基本资料_#{Digest::MD5.hexdigest(JSON.pretty_generate(字段定义))}"], 更新值:定义及文档.to_json)