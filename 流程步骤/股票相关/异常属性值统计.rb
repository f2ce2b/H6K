
判断条件 = {
  '基础数据': {
    '或': [
      ['09-上市日期', '等于', ['', nil]],
      ['A股简称', '包含', 'ST股'],
      ['证券类别', '包含', '风险警示'],
    ]
  },
  '基本面-指标最新数据':{
    '或': [
      [['扣非每股收益(元)', '稀释每股收益(元)', '每股公积金(元)', '每股未分配利润(元)', '每股经营现金流(元)'], '等于', 0]
    ]
  },
  # '基本面-指标对比数据':{
  #   '或': [
  #     [['毛利率(%)_最新数据', '毛利率(%)_上年同期', '加权净资产收益率(%)_最新数据', '加权净资产收益率(%)_上年同期'], '等于', [0, '', nil]]
  #   ]
  # }
}

文件内容 = {}
存储文件地址 = [@运行参数['目标目录'], "异常属性值统计.json"].join('/')

统计内容 = lambda do |哈希:|
  内容 = {
    'A股代码': J结果集.find_by(键: 'A股代码'),
    '公司名称': J结果集.find_by(键: '公司名称'),
    '所属行业': JSON.parse(J结果集.find_by(键: '所属东财行业')).join('/'),
    '上市日期': J结果集.get(层级: ['打新','摘要','上市日期'], 格式化: false)[1],
    '区域': J结果集.find_by(键: '区域'),
    '公司网址': J结果集.find_by(键: '公司网址'),
  }
  case 哈希['属性']
  when '证券类别'
    内容['资产负债率'] = JSON.parse(J结果集.find_by(键: '资产负债率'))
    内容['经营范围'] = JSON.parse(J结果集.find_by(键: '经营范围'))
  end
  内容
end

判断条件.each do |名称, 条件|
  res = L逻辑.判断(条件: 条件)
  next unless res[0]

  res[1][true].each do |哈希|
    文件内容[[哈希['属性'], 哈希['符号'], 哈希['对比值']].join] = 统计内容.call(哈希: 哈希)
  end

end

return if 文件内容 == {}
# 不用主动删除 每次运行前会根据存储结构删除未注册文件

File.open(存储文件地址, 'w'){|f| f.write(文件内容.to_json)}
