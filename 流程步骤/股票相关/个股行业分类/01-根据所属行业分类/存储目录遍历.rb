require_relative '../../../../面向对象/股票/加载.rb'

# 集群运算模式下无法根据 股票存储目录 重新分类所以跳过
return if @运行参数['股票存储目录'].nil? && Dir["#{@运行参数['目标目录']}/*"].size != 0

断点调试(binding) if @运行参数['股票存储目录'].nil?

行业分类 = {}

Dir["#{@运行参数['股票存储目录']}/*"].sort.each do |文件夹|
  存储目录 = StoreDir.new(目标目录: 文件夹)
  内容 = 存储目录.查找(文件名: '所属东财行业')

  next if Stock.异常代码.include?(文件夹.split('/')[-1])
  next if 存储目录.查找(文件名: '证券类别').include?('B股')

  断点调试(binding) unless 内容.is_a?(Array)
  断点调试(binding) unless 内容.size == 3
  分类名 = 内容[-1].gsub('(', '（').gsub(')', '）')

  (行业分类[分类名]||=[]) << 文件夹
end

FileUtils.rm_r(Dir["#{@运行参数['目标目录']}/*"])

行业分类.each do |名称, 数组|
  行业目录 = [@运行参数['目标目录'], 名称].join('/')
  Dir.mkdir(行业目录)
  数组.map{|dir| FileUtils.symlink(dir, [行业目录, dir.split('/')[-1]].join('/'))}
end