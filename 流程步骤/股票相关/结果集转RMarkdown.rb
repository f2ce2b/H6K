require_relative '../../面向对象/Ruby/Ruby.rb'

# 克隆的对象在改变对象值的时候也会改变原始值 具体原因未查
# 内容 = J结果集.内容['/'].clone

# 内容 = Hash[J结果集.内容['/'].clone].clone

# 内容 = {}
# J结果集.内容['/'].each do |键,值|
#   内容[键] = 值.clone
# end

# 严重问题!!! 上面无论如何怎样克隆都会改变结果集值(迭代格式化内容方法内有改变键值的代码)
# 猜想可能clone对于对象属性无效 
# 这个问题可能在不经意间其他地方造成很难排查的恶性后果
# 可是结果集#内容方法涉及范围较广 后面有时间解决
内容 = File.open([@运行参数['目标目录'], ".结果集.json"].join('/')){|file| JSON.parse(file.read)}['内容']['/']

内容.delete_if{|k, v| k =~ /.*[ElasticSearch].*/}

过滤空文件夹 = lambda do |哈希:|
  哈希.each do |键, 值|
    case 值
    when Hash
      if 值 == {}
        哈希.delete_if{|k, v| k == 键}
      else
        过滤空文件夹.call(哈希: 值)
      end
    end
  end
end

过滤空文件夹.call(哈希: 内容)
# 第二遍过滤过滤后的内容
过滤空文件夹.call(哈希: 内容)

迭代末级主体 = lambda do |哈希:, 键组: [], 结果: {}|
  哈希.each do |键, 值|
    case 值
    when Hash
      if 值.values.select{|value| [String, NilClass, Float, Integer].include?(value.class)}.size != 0
        # 结果键 = [键组, 键].flatten.map{|a| a.gsub(/\d{2}\-/, '')}
        结果键 = [键组, 键].flatten.map{|a| a.gsub(/\d{2}\-/, '')}.join('/')
        结果[结果键] =  值.clone.map{|k, v| [k.gsub(/\d{2}\-/, ''), (JSON.parse(v) rescue v)]}.to_h
      else
        迭代末级主体.call(哈希: 值, 键组: [键组, 键], 结果: 结果)
      end
    end
  end
  结果
end

result = 迭代末级主体.call(哈希: 内容.clone, 键组: [])

迭代格式化内容 = lambda do |哈希:|
  case 哈希
  when Hash
    # Hash.new(哈希).to_h.each do |键, 值|
    哈希.clone.each do |键, 值|
      if 键 =~ /\d{2,}\-.*/
        新键 = 键.gsub(/\d{2,}\-/,'')
        哈希.delete_if{|k,v| k == 键}
        哈希[新键] = 值
      end
      迭代格式化内容.call(哈希: 值)
    end
  end
end

迭代格式化内容.call(哈希: result)

迭代格式化值为空 = lambda do |哈希:|
  case 哈希
  when Hash
    哈希.each do |键, 值|
      哈希[键] = '暂无' if 值.nil?
      迭代格式化值为空.call(哈希: 值)
    end
  end
end

迭代格式化值为空.call(哈希: result)

res = Rmd.渲染(模板: '个股信息', 数据: result, 标题: @运行参数['目标目录'].split('/')[-1])
断点调试(binding) unless res[0]