require_relative '../../../面向对象/股票/加载.rb'

字典 = {
  '股票代码': 'SECUCODE',
  '股票简称': 'SECURITY_NAME',
  '发行价': 'ISSUE_PRICE',
  '最新价 ': 'LATELY_PRICE',
  '网上': {
    '发行中签率(%)': 'ONLINE_ISSUE_LWR',
    '有效申购股数(万股)': 'ONLINE_VA_SHARES',
    '有效申购户数(户)': 'ONLINE_VA_NUM',
    '网上超额认购倍数': 'ONLINE_ES_MULTIPLE',
  },
  '网下':{
    '配售中签率(%)': 'OFFLINE_VAP_RATIO',
    '有效申购股数(万股)': 'OFFLINE_VATS',
    '有效申购户数(户)': 'OFFLINE_VAP_OBJECT',
    '网下配售认购倍数': 'OFFLINE_VAS_MULTIPLE',
  },
  '总发行数量(万股)': 'TOTAL_ISSUE_NUM',
  '开盘溢价(%)': 'LD_OPEN_PREMIUM',
  '首日涨幅(%)': 'LD_CLOSE_CHANGE',
  '上市日期': 'LISTING_DATE',
}

对象翻译 = lambda do |对象:, 字典:, 结果:{}|
  case 字典
  when Hash
    字典.each do |键, 值|
      结果[键.to_s] = 对象翻译.call(对象: 对象, 字典: 值, 结果:(值.is_a?(Hash) ? 结果[键.to_s]={} : 结果[键.to_s]=nil))
    end
  when String
    结果 = case 字典
    when 'LISTING_DATE'
      (对象[字典]||'').split(' ')[0]
    else
      对象[字典]
    end
  else
    断点调试(binding)
  end
  结果
end

# 问题: 查询时间的自动调整
逐页请求 = lambda do |当前页数: 1|
  url = "https://datacenter-web.eastmoney.com/api/data/v1/get?sortColumns=LISTING_DATE%2CSECURITY_CODE&sortTypes=-1%2C-1&pageSize=50&pageNumber=#{当前页数}&reportName=RPTA_APP_IPOAPPLY&quoteColumns=f2~01~SECURITY_CODE%2Cf14~01~SECURITY_CODE&quoteType=0&columns=ALL&source=WEB&client=WEB&filter=((APPLY_DATE%3E%271991-01-01%27)(%7C%40APPLY_DATE%3D%22NULL%22))((LISTING_DATE%3E%271991-01-01%27)(%7C%40LISTING_DATE%3D%22NULL%22))"

  result = D东方财富.网络请求(链接: url, 请求模式: '直接请求')
  断点调试(binding) unless result[0]

  result[1]['result']['data'].each do |item|
    结果 = 对象翻译.call(对象:item, 字典:字典)
    res = @股票.转存储结构(层级: ['打新', '摘要'], 对象: 结果)
    断点调试(binding) unless res[0]
    J结果集.内容['/'][结果['股票代码']] = res[2]['层级结构']
  end

  [true, {"总页数" => result[1]['result']['pages'], "当前页数" => 当前页数}]
end

res = 逐页请求.call()
断点调试(binding) unless res[0]

(res[1]['当前页数']+1 .. res[1]['总页数']).each do |页码|
  res = 逐页请求.call(当前页数: 页码)
  断点调试(binding) unless res[0]
end

J结果集.更新
