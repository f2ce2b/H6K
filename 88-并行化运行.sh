[ ! "$TOP" ] && export TOP="$(dirname $(readlink -f $0))";

params=$1
echo "[$(date '+%Y-%m-%d %H:%M:%S.%3N')] 开始运行: "$params

params=$(jq -c --arg flag true '.["并行模式"] = $flag' <<<$1)

source "$TOP/面向对象/Shell/并行化运行.sh" "$params"
[ ! $? -eq 0 ] && echo '[false, "返回结果错误"]' && exit 255;

echo '[true, ""]'
exit 0

# bash 88-并行化运行.sh '{"目标流程":"股票数据缓存","目标目录": "/home/ubuntu/nfs/data/股票"}'
# bash 88-并行化运行.sh '{"目标流程":"个股分析","目标目录": "/home/ubuntu/nfs/data/股票"}'
# bash 88-并行化运行.sh '{"目标流程":"细分行业分析","目标目录":"/home/ubuntu/nfs/data/行业分类"}'