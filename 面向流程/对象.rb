require_relative '../面向对象/Ruby/Ruby.rb'

class FaceFlow < Dobject
  attr_accessor :执行步骤

  def initialize(**params)
    current_path = File.expand_path(File.dirname(__FILE__))
    # params[:属性] = "#{current_path}/属性"
    params[:方法] = "#{current_path}/方法"
    params[:流程步骤] ||= current_path+'/../流程步骤'
    params[:注册流程] ||= current_path+'/注册流程.json'
    super(**params)

    self.注册流程 = File.open(self.注册流程){|file| JSON.parse(file.read)}
    self.执行步骤 = []
  end

  def 初始化执行文件
    res = L流程.执行流程(流程: self.注册流程, 对象: self)
    断点调试(binding) if (!res[0] && res[1].to_i == 0)
    return [true, ''] if res[1] == 200

    self.执行步骤 = self.执行步骤.flatten
    self.执行步骤.delete_if{|item| File::directory?(item)}
    断点调试(binding) unless @目标目录

    执行内容 = ["require_relative '#{File.expand_path(File.dirname(__FILE__))}/../面向对象/Ruby/Ruby.rb'"]
    self.执行步骤.each do |file| 
      执行内容 << 'print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] "'
      执行内容 << "print '开始执行: #{file.gsub(self.流程步骤, '').gsub(/^\//, '')}'"
      执行内容 << "print \"\\n\""
      执行内容 << "require_relative '#{file}'" 
      执行内容 << "exit 255 unless ($?.exitstatus rescue 断点调试(binding)) == 0"
    end
    执行内容 << 'print "[true, \"\"]\n"'
    文件路径 = "#{@目标目录}/.require_relative.rb" 
    File.open(文件路径,'w'){|file| file.write(执行内容.join("\n")) }

    [true, 文件路径]
  end

  def method_missing(sym, *args)
    dir_or_files = Dir["#{self.流程步骤}/**/**"].sort.select{|dir_or_file| dir_or_file.include?("/#{sym}")}
    断点调试(binding) if dir_or_files.size == 0
    self.执行步骤 << dir_or_files
    [true, '']
  end

end