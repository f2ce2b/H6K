module ClusterWeight
  def 权重计算
    self.设备列表.each do |操作目标, 详情|
      while !(状态||=false) do
        状态, 日志 = self.日志读取(操作目标: 操作目标, 日志类型: '任务时耗')
        self.性能测试 if 状态 == false && 日志 == 404
      end

      # 单任务时耗 = 日志.map{|info| (info['结束时间'] - info['开始时间']).to_f / info['任务数量']}
      # 详情['平均时耗'] = 单任务时耗.sum.to_f/单任务时耗.size
      样本 = 日志.select{|info| info['平均时耗'] != nil}
      详情['平均时耗'] = 样本.map{|info| info['平均时耗']}.sum.to_f/样本.size
      断点调试(binding) if 详情['平均时耗'] == 0
    end

    最大时耗 = self.设备列表.map{|k,v| v['平均时耗']}.max

    self.设备列表.each do |操作目标, 详情|
      # 目前该规则有待完善的地方在于权重最低的分配数量最少 但是耗时是最长的
      详情['权重分数'] = 最大时耗.to_f/详情['平均时耗']
      # 线程数参与进来会导致线程数少但单核性能强的机器权重被拉低
      # 详情['权重分数'] = (最大时耗.to_f/详情['平均时耗'])*详情['线程数']
    end

    self.设备列表 = self.设备列表.sort_by{|k,v| v['权重分数']}.to_h
  end
end