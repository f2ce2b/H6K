module ClusterLog

  def 日志记录(日志类型:, 操作目标:, 写入:)
    日志目录 = File.join(self.日志目录地址, 日志类型, 操作目标, self.任务标识)
    FileUtils.mkdir_p(日志目录) unless File::directory?(日志目录)

    日志文件 = File.join(日志目录, self.归档时间)
    内容 = JSON.parse((File.open(日志文件){|文件对象| 文件对象.read} rescue '{}'))
    内容.merge!(JSON.parse(写入.to_json))
    File.open(日志文件, 'w'){|文件对象| 文件对象.write(JSON.pretty_generate(内容))}    
  end

  def 日志读取(日志类型:, 操作目标:)
    日志目录 = File.join(self.日志目录地址, 日志类型, 操作目标, self.任务标识)
    return [false, 404] unless File::directory?(日志目录)
    return [false, 404] if Dir["#{日志目录}/*"].size == 0

    内容 = Dir["#{日志目录}/*"].map{|path| JSON.parse((File.open(path){|文件对象| 文件对象.read} rescue 断点调试(binding)))}
    [true, 内容]
  end

end