require_relative '../面向对象/Ruby/Ruby.rb'
require_relative './日志.rb'
require_relative './权重.rb'
require_relative './命令.rb'
require_relative './注册.rb'

class FaceCluster < Base
  include ClusterLog
  include ClusterWeight
  include ClusterCommand
  include ClusterReg

  def initialize(**params)
    任务标识 = Digest::MD5.hexdigest(params.to_json)
    挂载目录 = '/home/ubuntu/nfs'
    params.merge!({
      任务标识: 任务标识,
      归档时间: Time.now.strftime("%Y-%m-%dT%H:%M:%S.%L"),
      设备列表: self.可用设备,
      日志目录地址: "#{挂载目录}/log",
      任务目录: [挂载目录, 任务标识].join('/'),
      挂载目录: 挂载目录,
    })

    super(**params)
  end

end