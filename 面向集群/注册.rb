module ClusterReg

  def 注册设备
    tmpfile_register = '/tmp/ClusterReg_register.env'
    return JSON.parse(File::open(tmpfile_register){|f|f.read}) if File::file?(tmpfile_register)

    register = File.open(File.join([File.expand_path(File.dirname(__FILE__)), '注册.json'])){|f| f.read}

    File::open(tmpfile_register, 'w'){|f|f.write(register)}
    return JSON.parse(register)
  end

  def 在线设备
    tmpfile_online = '/tmp/ClusterReg_online.env'
    return JSON.parse(File::open(tmpfile_online){|f|f.read}) if File::file?(tmpfile_online)

    online = {}
    self.注册设备['设备列表'].each do |mac地址, 详情|
      `nmap -p 22 #{详情['IPV4地址']} 1>/dev/null 2>/dev/null`
      next unless $?.exitstatus == 0
      online[详情['IPV4地址']] = 详情
    end

    File::open(tmpfile_online, 'w'){|f|f.write(online.to_json)}
    return online
  end

  def 可控设备
    tmpfile_steerable = '/tmp/ClusterReg_steerable.env'
    return JSON.parse(File::open(tmpfile_steerable){|f|f.read}) if File::file?(tmpfile_steerable)

    steerable = {}
    self.在线设备.each do |ipv4地址, 详情|
      `ssh -o PasswordAuthentication=no -o StrictHostKeyChecking=no #{详情['系统用户名']}@#{详情['IPV4地址']} "echo 'Hello Word.'" > /dev/null 2>&1`
      next unless $?.exitstatus == 0
      steerable[[详情['系统用户名'], 详情['IPV4地址']].join('@')] = 详情
    end

    File::open(tmpfile_steerable, 'w'){|f|f.write(steerable.to_json)}
    return steerable
  end

  def 可用设备
    tmpfile_usable = '/tmp/ClusterReg_usable.env'
    return JSON.parse(File::open(tmpfile_usable){|f|f.read}) if File::file?(tmpfile_usable)

    usable = {}
    self.可控设备.each do |ipv4地址, 详情|
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 开始检查: #{ipv4地址}\n"

      # 经测试在容器启动后尚未完成默认命令情况下 其他终端仍可以执行其他命令
      # 除非包含新增必要插件尚未完成安装等情况 否则在目前已知情况下没有其他影响
      # 不再使用项目专用容器而是使用init项目提供通用容器,目的在于解决上面存在情况
      output = `ssh -o PasswordAuthentication=no -o StrictHostKeyChecking=no #{详情['系统用户名']}@#{详情['IPV4地址']} "docker ps | grep self.container.system"`
      断点调试(binding, 备注: "该节点容器未启动,检查容器运行日志") unless $?.exitstatus == 0
      断点调试(binding) unless output.include?('self.container.system')

      # init项目提供容器默认工作文件夹为workdir(或者 exec -w 指定工作文件夹)
      output = `ssh -o PasswordAuthentication=no -o StrictHostKeyChecking=no #{详情['系统用户名']}@#{详情['IPV4地址']} "docker exec self.container.system bash -lc 'rm -rf $HOME/workdir && ln -s /home/ubuntu/nfs/2.0-并行化改造 $HOME/workdir'"`
      断点调试(binding) unless $?.exitstatus == 0

      usable[[详情['系统用户名'], 详情['IPV4地址']].join('@')] = 详情
    end

    File::open(tmpfile_usable, 'w'){|f|f.write(usable.to_json)}
    
    断点调试(binding) if usable == {} # 可用节点为空
    return usable
  end

end