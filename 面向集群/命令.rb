module ClusterCommand
  def 性能测试
    待分配任务 = Dir["#{self.目标目录}/*"]

    self.设备列表.each do |操作目标, 详情|
      断点调试(binding) if 待分配任务.size == 0
      详情['设备分配目录'] = 待分配任务.slice!(0, (详情['并行使用线程数'] || (详情['线程数']- 1)))
      详情['任务数量'] = 详情['设备分配目录'].size
      详情['性能测试'] = true
    end

    load File.expand_path([File.dirname(__FILE__), '../流程步骤/面向集群/集群运算/99-结束任务/01-主节点/99-任务目录删除.rb'].join('/'))
    load File.expand_path([File.dirname(__FILE__), '../流程步骤/面向集群/集群运算/03-安排设备/02-环境准备/01-任务节点目录构建.rb'].join('/'))

    self.任务执行(额外参数: {'性能测试': true})

    load File.expand_path([File.dirname(__FILE__), '../流程步骤/面向集群/集群运算/99-结束任务/01-主节点/99-任务目录删除.rb'].join('/'))
  end

  def 任务执行(额外参数: {})
    参数 = {
      # "并行模式": true, 
      "目标流程": self.节点任务,
    }.merge!(额外参数)

    self.设备列表.each do |操作目标, 详情|
      params = {"目标目录": 详情['任务目录']}.merge!(参数)
      params.merge!({"并行使用线程数": 详情['并行使用线程数']}) if 详情['并行使用线程数']

      命令 = [
        # ['export', 'GEM_HOME=~/.gems'],
        [
          'bash', 
          ((self.多线程模式 rescue false) ? '88-并行化运行.sh' : '55-运行.sh'),
          %Q|'#{params.to_json.gsub('"', '\"')}'|,
        ]
      ]

      fork do
        开始时间 = Time.now.to_f
        output = FC.执行命令(说明: '执行节点具体任务', 命令: 命令, 操作目标: 操作目标)
        if output[-1].include?('[true')
          结束时间 = Time.now.to_f
          FC.日志记录(日志类型: '任务时耗', 操作目标: 操作目标, 写入: {
            '开始时间': 开始时间,
            '结束时间': 结束时间,
            '任务数量': 详情['任务数量'],
            '总时耗': (结束时间 - 开始时间),
            '平均时耗': (结束时间 - 开始时间) / 详情['任务数量'],
          })
        else
          File.open([self.任务目录, "#{操作目标}.log"].join('/'), 'w'){|文件对象| 文件对象.write(output.join("\n"))}
        end
    
      end
    
    end
    
    Process.waitall

    unless Dir["#{self.任务目录}/*.log"].size == 0
      断点调试(binding) if Dir["#{self.任务目录}/*.log"].map{|p| File.open(p){|c|c.read}.include?('false')}.include?(true)
    end
  end

  def 执行命令(说明:, 命令:, 操作目标:)
    cmd = if 命令.map{|cmds| cmds.is_a?(Array)}.uniq == [true]
<<-CMD
<< "EOF"
docker exec self.container.system bash -cl "#{命令.map{|cmds| cmds.join(' ')}.join(";\n")}"
EOF
CMD
    else
      %Q|docker exec self.container.system bash -cl #{命令.join(' ')}|
    end

    print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 开始执行: #{说明}\n"
    shell = %Q|ssh -o StrictHostKeyChecking=no -T #{操作目标} #{cmd}|
    print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 具体命令: #{shell}\n"
    output = `#{shell}`
    result = $?
    unless result.exitstatus == 0
      print output
      断点调试(binding) 
    end
    # print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 返回结果: #{output == '' ? '空' : "\n#{output}"}"
    print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 返回结果: \n#{output}"
    output.split("\n")
  end
end