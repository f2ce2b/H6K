#!/bin/bash --login

sudo apt update

# mount.nfs 辅助程序
sudo apt install -y nfs-common

# 并行执行命令工具
sudo apt install -y parallel

# Redis相关
sudo apt install -fy redis-tools

# R语言相关
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9

# # deepin
# # https://blog.csdn.net/weixin_45503019/article/details/108637980
# # https://blog.csdn.net/weixin_44523175/article/details/108445092

# sudo sh -c "echo 'deb [by-hash=force] https://mirrors.tuna.tsinghua.edu.cn/deepin panda main contrib non-free' >> /etc/apt/sources.list"
# sudo sh -c "echo 'deb [by-hash=force] https://mirrors.aliyun.com/deepin/ panda main contrib non-free' >> /etc/apt/sources.list"
# sudo sh -c "echo 'deb http://cloud.r-project.org/bin/linux/debian buster-cran40/' >> /etc/apt/sources.list"

# R_LIBS_USER=${R_LIBS_USER-'~/R/$platform-library/4.0'}
# R_LIBS_SITE=${R_LIBS_SITE-'/usr/local/lib/R/site-library:/usr/lib/R/site-library:/usr/lib/R/library'}

# # apt-key adv --keyserver keys.gnupg.net --recv-key 'E19F5F87128899B192B1A2C2AD5F960A256A04AF'
# sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E19F5F87128899B192B1A2C2AD5F960A256A04AF

# sudo apt-cache rdepends r-base-core #查看一下
# sudo apt update

# # 检查是否存在   由于没有公钥，无法验证下列签名： NO_PUBKEY B8F25A8A73EACF41
# # sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys B8F25A8A73EACF41

# #可能提示有更新，执行下面第一条命令
# sudo apt-get dist-upgrade
# sudo apt install -t buster-cran40 r-base

# ubuntu
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/'
sudo apt install -fy r-base

# r-base-core : 依赖: libicu66 (>= 66.1-1~) 但无法安装它
# http://security.ubuntu.com/ubuntu/pool/main/i/icu/libicu66_66.1-2ubuntu2_amd64.deb

sudo chown -R $USER /usr/local/lib/R/site-library
sudo apt-get install -y libssl-dev libcurl4-openssl-dev

# R插件依赖
sudo apt-get install -y pandoc
sudo apt-get install -y libssl-dev libcurl4-openssl-dev
# 资源文件夹
sudo mkdir -p /home/ubuntu/nfs/data
chown -R $USER /home/ubuntu
