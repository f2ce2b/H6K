#!/bin/bash --login
# 目前该并行化存在的缺点在于无法断点调试
[ ! "$TOP" ] && export TOP="$(dirname $(readlink -f $0))/../../";
export params=$1

jq '.' -e <<< "$1"
[ ! $? -eq 0 ] && echo '[false, "参数格式错误"]' && exit 255;

target_path=$(jq -r '.["目标目录"]' <<< "$1")

[ ! -d "$target_path" ] && echo '[false, "并行化启动前目标目录不存在", "'$target_path'", "'$params'"]' && exit 255;

finished_log="$target_path/.$(date '+%Y-%m-%d').log"

[ ! -f "$finished_log" ] && (touch $finished_log || exit 255);

total_count=$(ls "$target_path" | wc -l)

function report_error_logs {
  error_logs=($(find $target_path/*/.异常日志.log 2>/dev/null | sort))
  if [ ${#error_logs[@]} -ne 0 ];then
    for index in "${!error_logs[@]}";do
      file_name=${error_logs[$index]}
      echo -e "\n$file_name"
      cat $file_name
    done
    rm -f ${error_logs[@]}
    echo '[false, "运行异常"]'
  else
    rm -f $finished_log
    echo '[true, "全部成功运行"]'
  fi
}

trap report_error_logs EXIT
trap report_error_logs KILL
trap report_error_logs INT
trap "" SIGINT # Ctrl-c 退出不做任何操作

function exec {
  result=$(grep "${1##*/}" "$2")
  [ "$result" ] && return ;

  error_file="$1/.异常日志.log"
  item_params=$(jq -c --arg target_dir $1 '.["目标目录"] = $target_dir' <<<$params)
  echo "[$(date '+%Y-%m-%d %H:%M:%S.%3N')] 开始运行: "$item_params

  source "$TOP/55-运行.sh" "$item_params" >$error_file 2>&1
  code=$?

  if [ ! $code -eq 0 ];then
    echo -e "\e[0;31m"'[false, "运行时异常,详情查看日志文件.", "'$1'/.异常日志.log"]'"\e[0;0m"
    kill -9 $(ps -ef | awk '/parallel/{print $2}')
  else
    handle_count=$(wc -l $2 | awk '/ /{print $1}')
    let handle_count=handle_count+1
    echo '[true, "'$handle_count'/'$3'", "'$(date '+%Y-%m-%d %H:%M:%S.%3N')'", "'$1'"]' | tee -a "$2"
    rm -f $error_file
  fi
}
export -f exec
arguments=($finished_log $total_count)

# 使用比例分配并行线程数 线程越多空闲也越多 20线程的3/4会有5个空闲
# default_threads=$(num_threads=$(nproc); echo $((num_threads * 3 / 4)))
default_threads=$(num_threads=$(nproc); echo $((num_threads - 1)))
parallel_threads=$(jq -r --arg default_threads $default_threads '.["并行使用线程数"]//$default_threads' <<< "$params")

# work_dirs=($(find "$target_path" -maxdepth 1 -type d,l | tail -n +2 | sort | head -n 30))
work_dirs=($(find "$target_path" -maxdepth 1 -type d,l | tail -n +2 | sort))
# echo "[$(date '+%Y-%m-%d %H:%M:%S.%3N')] 并行文件夹: "${work_dirs[@]}

parallel -j $parallel_threads exec {} ${arguments[@]} ::: ${work_dirs[@]}