require_relative '../Ruby/Ruby.rb'

class Stock < Base
  attr_accessor :结构路径

  def initialize(**params)
    super(**params)

    if self.class.异常代码.include?(self.代码)
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 异常代码: #{self.代码}.\n"
      exit 0
    end
    
    self.结构路径 ||= File.expand_path([File.dirname(__FILE__), './存储结构'].join('/'))
  end

  def 存储结构
    res = JSON.parse(`tree -J #{self.结构路径}`)[0]
    result = {}

    递归处理 = lambda do |对象, 结果, 路径=[]|
      # tree 1.8和2.0区别在于2.0中空白文件夹不在有contents空数组
      if 对象['type'] == 'directory' && (对象['contents']||[]).length > 0
        对象['contents'].each do |content|
          递归处理.call(content, (结果[对象['name']] ||= {}), [路径, 对象['name']].flatten)
        end
      elsif (对象['type'] == 'directory')
        结果[对象['name']] = {}
      elsif 对象['type'] == 'file'
        结果[对象['name']] = File.open([路径, 对象['name']].flatten.join('/')){|file| file.read}
      elsif 对象['type'] == 'link'
        # 结果[对象['name']] = 对象['target']
        断点调试(binding)
      end
    end

    递归处理.call(res, result)

    [true, result.values[0]]
  end

  def 转存储结构(层级:, 对象:, 结构: self.存储结构[1], 结构层级: [])
    if 层级[0]
      层级 = 层级.clone
      相关结构 = 结构.select{|键,值| 键.include?(层级[0])}
      断点调试(binding) if 相关结构.keys.size != 1
      层级.delete_at(0)
      return 转存储结构(层级: 层级, 对象: 对象, 结构: 相关结构.values[0], 结构层级: [结构层级, 相关结构.keys.first].flatten)
    end

    result = {}

    迭代赋值 = lambda do |相关结构:, 相关对象:, 赋值对象:|
      相关结构.each do |键,值|
        相关结果 = 相关对象.select{|k,v| k.include?(键.split('-')[-1])}
        断点调试(binding) if 相关结果.values.size != 1
        赋值对象[键] = case 值
        when Hash
          相关结果_ = 相关对象.select{|k,v| k.include?(键.split('-')[-1])}
          断点调试(binding) if 相关结果_.values.size != 1
          赋值对象[键]=迭代赋值.call(相关结构: 值, 相关对象: 相关结果_.values[0], 赋值对象: {})
        when String
          相关值 = 相关结果.values[0]
          case 相关值
          when Hash, Array
            相关值.to_json
          else
            相关值
          end
        else
          断点调试(binding)
        end
      end

      赋值对象
    end

    迭代赋值.call(相关结构: 结构, 相关对象: 对象, 赋值对象: result)

    断点调试(binding) if result == {}
    断点调试(binding) if result.keys.size != 对象.keys.size

    结果 = {}
    数组转层级哈希 = lambda do |层级, 当前结果, 最终值|
      if 层级.size == 1
        return 当前结果[层级[0]] = 最终值
      else
        当前结果 = 当前结果[层级[0]] = {}
        层级.delete_at(0)
        数组转层级哈希.call(层级, 当前结果, 最终值)
      end
    end
    
    数组转层级哈希.call(结构层级.clone, 结果, result)

    [true, result, {'结构层级' => 结构层级, '层级结构' => 结果}]
  end

  def self.异常代码
    [
      # 已退市
      '002257.SZ', 
      '000666.SZ',

      # 营业收入为空
      '688302.SH', 
      "688192.SH", 
      "688373.SH",
      "688382.SH",
      
    ]
  end

  def 是否解析?
    # binding.pry
    return [false, '尚未上市'] if ['', nil].include?(J结果集.get(层级: ['打新','摘要','上市日期'], 格式化: false)[1])
    return [false, 'ST股'] if J结果集.find_by(键: 'A股简称') =~ /^\*ST.*/

    证券类别 = J结果集.find_by(键: '证券类别')
    return [false, 证券类别] if 证券类别 =~ /风险警示/
    [true, '']
  end

end