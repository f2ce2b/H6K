---
title: "数据统计"
date: "`r format(Sys.time(), '%Y-%m-%d %H:%M:%OS3')`"

# 以html或word格式输出
output: 
  html_document:
    theme: cerulean
    toc: true
    toc_depth: 6
    toc_float:
      true
    number_sections: true
---

<style type="text/css">
  .main-container {
    max-width: 100% !important;
    margin: auto;
  }
</style>

<script>
  // 在页面加载完成后执行该函数
  window.onload = function() {
    // 将所有表格中带有http或https的内容改为a标签
    tds = document.querySelectorAll("td");
    tds.forEach((element) => {
      if (/http[s]*\:\/\/.*/.test(element.textContent)) {
        a = document.createElement("a");
        a.href = element.textContent;
        a.target = '_blank'
        a.textContent = element.textContent;
        element.innerHTML = a.outerHTML;
      }
    });
  };
</script>

# 股票

## 企业基本信息

### 图表

```{r, echo=FALSE}
绘制('企业基本信息')
```

### 官网

```{r, echo=FALSE}
绘制('企业官网信息')
```

## 企业详细信息

### 图表

```{r, echo=FALSE}
绘制('企业详细信息')
```

## 风险警示企业

### 图表

```{r, echo=FALSE, out.width='100%', out.height=900}
绘制('风险警示企业')
```

```{r, echo=FALSE, out.width='100%', out.height=900}
绘制('所属地域')
```

# 行业

## 描述性统计

### 图表

```{r, echo=FALSE}
绘制('行业描述性统计')
```

# 基金

## 基金历史净值

### 图表

```{r, echo=FALSE, out.width='100%'}
绘制('基金历史净值')
```
