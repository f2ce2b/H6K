# Rscript R.r '{"参数文件": "/home/ubuntu/2T/space/资源体系/数据文件/金融/基于国内开放平台数据的采集与应用/2.0-并行化改造/面向对象/R/测试/数据统计.json", "立即绘制": true}'

if (!require('rjson', character.only = TRUE)) {
  install.packages('rjson', repos = "https://mirrors.ustc.edu.cn/CRAN/")
}
library(rjson)

RootPath = getwd()

requires = fromJSON(file=file.path(RootPath, '配置/依赖库.json'))

for (key in names(requires)) {
  current_time = format(Sys.time(), "[%Y-%m-%d %H:%M:%OS3]")
  cat(current_time, '')

  lib = requires[[key]]

  if (!require(lib, character.only = TRUE)) {
    install.packages(lib, repos = "https://mirrors.ustc.edu.cn/CRAN/")
  }

  if (!is.loaded(lib)) {
    library(lib, character.only = TRUE)
  }
}

# 加载通用方法
for (dir in list.dirs()) {
  # 设置工作目录
  setwd(file.path(RootPath, dir))
  for(file in list.files(pattern = "*.R$")){
    source(file)
  }
}

setwd(RootPath)
Methods = ls()

if (!length(commandArgs(TRUE)) > 0) {
  print(toJSON(c('false', '参数不能为空')))
  q()
}

args = commandArgs(T)
运行参数 = fromJSON(args[1])

必须参数 = list(
  c('运行参数', '参数文件', 'fromJSON', '参数文件内容'),
  c('参数文件内容', '绘制', 'set', '绘制内容'),
  c('参数文件内容', '参数', 'set', '参数内容')
)

for (array in 必须参数){
  tryCatch(
    assign('value', get(array[2], get(array[1]))),
    error = function(e) {
      print(toJSON(c('false', paste0(array[1], '中', array[2], '属性不能为空'))))
      q()
    }
  )

  if (array[3] == 'fromJSON'){
    assign(array[4], fromJSON(value))
  }else if (array[3] == 'set'){
    assign(array[4], value)
  }
}

绘制 = function (key) {
  params = get(key, 绘制内容)
  # 目前如何将多个图表(交互及静态)同时返回仍未解决
  # 所以目前尽管一个数据格式可以匹配多种图表
  # 但却只会返回最先匹配的一个
  # results = c()
  for (key in get('数据格式', params)) {
    for (method in Methods) {
      if(!grepl(key, method)) next
      result = do.call(method, list(params=params))
      return(result)
      # results = c(results, result)
    }
  }
  # return(results)
}

if (!is.null(运行参数$立即绘制) && get('立即绘制', 运行参数)) {
  for (输出格式 in 参数内容$文档转换$输出格式){
    rmarkdown::render(
      参数内容$文档转换$路径, 
      output_format = 输出格式, 
      encoding="UTF-8"
    )
  }
}

print(toJSON(c('true', '')))
