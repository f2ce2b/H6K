class ResultFile < Base
  attr_accessor :内容

  def initialize(**params)
    super(**params)
    self.内容 ||= {}
  end

  def get(层级: [], 内容: self.内容['/'], 格式化: true)
    if 层级[0]
      相关内容 = 内容.select{|键,值| 键.include?(层级[0])}
      return [true, {}] if 相关内容.keys.size == 0 && 层级.size == 1
      断点调试(binding) if 相关内容.keys.size != 1
      层级 = 层级.clone
      层级.delete_at(0)
      return self.get(层级: 层级, 内容: 相关内容.values[0], 格式化: 格式化)
    end

    return [true, 内容] unless 格式化
    [true, 内容.map{|k,v| [k.gsub(/\d{2}-/, ''), (JSON.parse(v) rescue v)]}.to_h]
  end

  def find_by(键:, 内容: self.内容['/'], 结果: [], 键组: [])
    case 内容
    when Hash
      内容.each do |key, value|
        if key.to_s.include?(键.to_s) || [键组, key.to_s].flatten.join('/') =~ Regexp.new(".*#{键}$")
          结果 << value 
        else
          结果 << self.find_by(键: 键, 内容: value, 键组: [键组, key.to_s].flatten)
        end
      end
    else
      # 暂无其他匹配条件
    end

    结果.delete_if{|i| i.nil?}
    断点调试(binding) if 结果.size > 1
    结果[0]
  end

  def 内容更新(层级:, 更新值:, 内容: self.内容['/'])
    相关内容 = 内容.select{|键,值| 键.include?(层级[0])}
    断点调试(binding) if 相关内容.keys.size > 1
    
    unless 层级.size == 1
      内容[层级[0]] = 相关内容 = {} if 相关内容.keys.size == 0
      层级 = 层级.clone
      层级.delete_at(0)
      return self.内容更新(层级: 层级, 更新值: 更新值, 内容: 相关内容.values[0])
    end

    实际键 = (相关内容.keys[0] || 层级[0])
    内容[实际键] = 更新值

    self.更新
  end

  def 更新
    content = {}
    self.instance_variables.each do |属性名|
      content["#{属性名.to_s.gsub('@', '')}"] = self.instance_variable_get(属性名)
    end

    File::open(self.文件路径, 'w') do |文件对象|
      文件对象.write(JSON.pretty_generate(content))
    end
  end

  def 删除结果存储文件
    File.delete(self.文件路径)
  end

  def 内容过滤及扁平化处理
    content = File.open([self.目标目录, ".结果集.json"].join('/')){|file| JSON.parse(file.read)}['内容']['/']

    过滤空文件夹 = lambda do |哈希:|
      哈希.each do |键, 值|
        case 值
        when Hash
          if 值 == {}
            哈希.delete_if{|k, v| k == 键}
          else
            过滤空文件夹.call(哈希: 值)
          end
        end
      end
    end
    
    过滤空文件夹.call(哈希: content)
    # 第二遍过滤过滤后的内容
    过滤空文件夹.call(哈希: content)
    
    扁平化处理 = lambda do |哈希:, 键组: [], 结果: {}|
      哈希.each do |键, 值|
        case 值
        when Hash
          if 值.values.select{|value| [String, NilClass, Float, Integer].include?(value.class)}.size != 0
            # 结果键 = [键组, 键].flatten.map{|a| a.gsub(/\d{2}\-/, '')}
            结果键 = [键组, 键].flatten.map{|a| a.gsub(/^\d{2}\-/, '')}.join('/')
            结果[结果键] =  值.clone.map{|k, v| [k.gsub(/^\d{2}\-/, ''), (JSON.parse(v) rescue v)]}.to_h
          else
            扁平化处理.call(哈希: 值, 键组: [键组, 键], 结果: 结果)
          end
        end
      end
      结果
    end
    
    result = 扁平化处理.call(哈希: content.clone, 键组: [])
    
    迭代格式化内容 = lambda do |哈希:|
      case 哈希
      when Hash
        哈希.clone.each do |键, 值|
          if (键 =~ /^\d{2,}\-.*/) && !(键 =~ /^\d{4}\-\d{2}\-\d{2}$/)
            新键 = 键.gsub(/^\d{2,}\-/,'')
            哈希.delete_if{|k,v| k == 键}
            哈希[新键] = 值
          end
          迭代格式化内容.call(哈希: 值)
        end
      end
    end
    
    迭代格式化内容.call(哈希: result)
  end

  def 存储域(当前路径, 起始词: )
    # [起始词, 当前路径.split(起始词)[1].split('/')].flatten
    空间地址 = 当前路径.split(起始词)[1].split('/')
    空间地址 = 空间地址.delete_if{|i| [nil, ''].include?(i)}
    空间地址 = 空间地址.map{|i| i.gsub(/^\d{2}\-|\.rb$/, '')}
    
    迭代地址 = lambda do |存储域: nil, 数组: |
      return 存储域 if 数组 == []
      存储键 = 数组.delete_at(0)
      迭代地址.call(存储域: (存储域[存储键] ||= {}), 数组: 数组)
    end

    迭代地址.call(存储域: self.内容['/'], 数组: 空间地址.clone)
  end

  def 扁平化结果
    循环迭代 = lambda do |结构:, 键组: [], 结果: {}|
      结构.each do |键,值|
        if 值.class == Hash && 值.values.map(&:class).include?(Hash)
          循环迭代.call(结构: 值, 键组: [键组, 键].flatten, 结果: 结果)
        elsif 值.class == Hash && 值.values.map(&:class).include?(String)
          值.each do |k,v|
            结果[[键组, 键, k].flatten.join('/')] = JSON.parse(v)
          end
        else
          断点调试(binding)
        end
      end
      结果
    end
    
    循环迭代.call(结构: self.内容['/'])
  end
end