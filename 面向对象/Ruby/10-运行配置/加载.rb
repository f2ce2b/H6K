配置文件路径 = File.expand_path([File.dirname(__FILE__), './配置文件.json'].join('/'))
基础配置 = File.open(配置文件路径){|file| JSON.parse(file.read)}
运行配置 = 基础配置.merge!(@运行参数)

# File.open(File::join([运行配置['目标目录'], '.运行配置.json']), 'w'){|file| file.write(JSON.pretty_generate(运行配置))}
require_relative './对象.rb'

@运行配置 = RunConfig.new(**Hash[运行配置.map{|(k,v)| [k.to_sym,v]}])
