class StoreDir < Base

  # 查找适用于根据关键词找到相关文件并返回内容
  def 查找(文件名:)
    结果 = Dir["#{self.目标目录}/**/*#{文件名}*"]
    断点调试(binding) if 结果.size != 1
    断点调试(binding) unless File.file?(结果[0])
    File.open(结果[0]){|f| content = f.read();(JSON.parse(content) rescue content)}
  end

  # 搜索适用于根据给出的关键词依次在上轮的范围内查找
  def 搜索(关键词:, 范围: [], 结果: [])
    关键词 = 关键词.clone
    # 初始环节
    if 范围 == []
      搜索词 = 关键词.delete_at(0)
      搜索范围 = Dir["#{self.目标目录}/#{搜索词}"].sort
      return self.搜索(关键词: 关键词, 范围: 搜索范围)
    # 结束环节
    elsif 关键词 == [] && 结果 != []
      return [true, {
        "结果" => 结果,
        "范围" => 范围,
      }]
    # 以下是中间过程匹配
    # 文件内容为Json格式
    elsif 关键词 != [] && 范围.is_a?(Array) && 范围.map{|i| File.file?(i)}.uniq == [true] && 结果 == []
      搜索词 = 关键词.delete_at(0)
      文件内容 = 范围.map{|i| File.open(i){|f| c=f.read; (JSON.parse(c) rescue c)}}
      断点调试(binding) unless 文件内容.map{|i| i.is_a?(Hash)}.uniq == [true]
      搜索结果 = 文件内容.map{|i| i[搜索词]}
      return self.搜索(关键词: 关键词, 范围: 范围, 结果: 搜索结果)
    # 关键词数组元素数量为1 返回文件内容
    elsif 关键词 == [] && 结果 == []
      文件内容 = (范围.map{|i| File.open(i){|f| c=f.read; (JSON.parse(c) rescue c)}} rescue 断点调试(binding))
      return self.搜索(关键词: 关键词, 范围: 范围, 结果: 文件内容)
    else
      断点调试(binding)
    end
    self.目标目录
  end

  def 存储结构(目标目录: self.目标目录)
    res = JSON.parse(`tree -J #{目标目录}`)[0]
    result = {}

    递归处理 = lambda do |对象, 结果, 路径=[]|
      if ['link', 'directory'].include?(对象['type']) && (对象['contents']||[]).length > 0
        对象['contents'].each do |content|
          递归处理.call(content, (结果[对象['name']] ||= {}), [路径, 对象['name']].flatten)
        end
      elsif ['link', 'directory'].include?(对象['type'])
        结果[对象['name']] = {}
      elsif 对象['type'] == 'file'
        结果[对象['name']] = File.open([路径, 对象['name']].flatten.join('/')){|file| file.read}
      else
        断点调试(binding)
      end
    end

    递归处理.call(res, result)

    [true, result.values[0]]
  end

  def 转存储结构(层级:, 对象:, 结构: self.存储结构[1], 结构层级: [])
    if 层级[0]
      层级 = 层级.clone
      相关结构 = 结构.select{|键,值| 键.include?(层级[0])}
      断点调试(binding) if 相关结构.keys.size != 1
      层级.delete_at(0)
      return 转存储结构(层级: 层级, 对象: 对象, 结构: 相关结构.values[0], 结构层级: [结构层级, 相关结构.keys.first].flatten)
    end

    result = {}

    迭代赋值 = lambda do |相关结构:, 相关对象:, 赋值对象:|
      相关结构.each do |键,值|
        相关结果 = 相关对象.select{|k,v| k.include?(键.split('-')[-1])}
        断点调试(binding) if 相关结果.values.size != 1
        赋值对象[键] = case 值
        when Hash
          相关结果_ = 相关对象.select{|k,v| k.include?(键.split('-')[-1])}
          断点调试(binding) if 相关结果_.values.size != 1
          赋值对象[键]=迭代赋值.call(相关结构: 值, 相关对象: 相关结果_.values[0], 赋值对象: {})
        when String
          相关值 = 相关结果.values[0]
          case 相关值
          when Hash, Array
            相关值.to_json
          else
            相关值
          end
        else
          断点调试(binding)
        end
      end

      赋值对象
    end

    迭代赋值.call(相关结构: 结构, 相关对象: 对象, 赋值对象: result)

    断点调试(binding) if result == {}
    断点调试(binding) if result.keys.size != 对象.keys.size

    结果 = {}
    数组转层级哈希 = lambda do |层级, 当前结果, 最终值|
      if 层级.size == 1
        return 当前结果[层级[0]] = 最终值
      else
        当前结果 = 当前结果[层级[0]] = {}
        层级.delete_at(0)
        数组转层级哈希.call(层级, 当前结果, 最终值)
      end
    end
    
    数组转层级哈希.call(结构层级.clone, 结果, result)

    [true, result, {'结构层级' => 结构层级, '层级结构' => 结果}]
  end

  def 写入结构(结构:)
    层级迭代 = lambda do |哈希:, 键组: []|
      层级地址 = File.join(键组.flatten.map(&:to_s))
      Dir.mkdir(层级地址) unless File::directory?(层级地址)
    
      哈希.each do |键, 值|
        case 值
        when Hash
          层级迭代.call(哈希: 值, 键组: [键组, 键])
        when String, NilClass, Float, Integer
          文件路径 = File.join([层级地址, 键].map(&:to_s)) 
          if File::file?(文件路径)
            md5_值 = Digest::MD5.hexdigest(值.to_s)
            文件内容 = File.open(文件路径){|file|file.read}
            md5_文件 = Digest::MD5.hexdigest(文件内容)
            next if md5_值 == md5_文件
            # 非特殊情况不更新现有文件 但似乎不太容易实现
            # 断点调试(binding) unless [nil, ''].include?(文件内容)
          end 
    
          File.open(文件路径, 'w') do |文件对象|
            begin
              文件对象.write(JSON.pretty_generate(JSON.parse(值)))  
            rescue => exception
              文件对象.write(值)
            end
          end
        else
          断点调试(binding)
        end
      end
    end

    结构.each do |键, 值|
      层级迭代.call(哈希: 值, 键组: [self.目标目录, 键])
    end
  end
end