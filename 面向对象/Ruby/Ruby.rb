#!/usr/bin/env ruby

当前路径 = File.expand_path(File.dirname(__FILE__))

requires = {
  "断点调试(binding)" => "pry",
  "Http请求"         => "rest-client",
  "JSON处理"         => "json",
  "文字转拼音"        => "ruby-pinyin",
  "Html文本解析"      => "nokogiri",
  "文件系统工具"      => "fileutils",
  "uri"             => "uri",
  "base64"          => "base64",
  "CGI接口实现"      => "cgi", # 主要用于链接内容转义
  # gruff的默认字体不支持中文
  # 依赖ImageMagick: sudo apt-get install -fy libmagickwand-dev
  # "绘制图表"         => "gruff",
# '生成图像'           => 'rmagick',

# '后台任务队列系统'     => 'resque',

# '终端工具'          => 'irb',
# '命令行选项分析'     => 'optparse',
# 'JSON转Ruby对象'    => 'ostruct',
# '格式化打印JSON数据' => 'awesome_print',
}

requires.each do |desc, gem_name|
  begin
    require gem_name
  rescue LoadError => e
    puts "缺少Gem: #{gem_name}(用于#{desc}), 正在尝试安装该Gem."
    `gem install #{gem_name}`
    断点调试(binding, 备注: "#{gem_name}安装失败") unless $?.exitstatus == 0
  end
end

class Base
  def initialize(**params)
    params.each do |属性名, 属性值|
      self.class.attr_accessor "#{属性名}"
      self.instance_variable_set("@#{属性名}", 属性值)
    end
  end

  def 数据结构类型(对象:)
    定义 = {
      '[[字符串,数字]]': {
        '别名': ['AASN'], # ArayArayStringNumber
        '判断条件': [
          lambda { |对象| 对象.is_a?(Array) },
          lambda { |对象| 对象.map { |item| item.is_a?(Array) }.uniq == [true] },
          lambda { |对象| 对象.map { |item| item.size }.uniq == [2] },
          lambda { |对象| 对象.map { |item| [item[0].class, item[1].class] }.uniq.flatten == [String, Integer] },
        ],
      },
      '[{键:值}]': {
        '别名': ['AHKV'],
        '判断条件': [
          lambda { |对象| 对象.is_a?(Array) },
          lambda { |对象| 对象.map { |item| item.is_a?(Hash) }.uniq == [true] },
        ],
      },
      '{键:值}': {
        '别名': ['HKV'],
        '判断条件': [
          lambda { |对象| 对象.is_a?(Hash) },
        ],
      },
    }

    定义.each do |名称, 定义|
      flag = 定义[:'判断条件'].each do |条件|
        break false unless 条件.call(对象)
      end

      next if flag == false
      return [名称.to_s, Hash[定义.map{|(k,v)| [k.to_s,v]}]]
    end

    断点调试(binding)
  end
end

@运行参数 = JSON.parse(ARGV[0])

def 断点调试(binding, 备注 = "无")
  return binding.pry unless (@运行参数 ||= JSON.parse(ARGV[0]))["并行模式"]
  # 目前并行模式下无法进行断点调试(binding) 只能提供相关信息后退出程序
  time = Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")
  print "[#{time}] 运行异常: 运行过程中遇到无法处理的异常,将退出程序等待后续排查.\n"
  message = [false, "运行异常", { 时间: time, 地点: caller, 原因: "运行过程中遇到无法处理", 备注: 备注, 参数: @运行参数 }]
  print JSON.pretty_generate(message)
  print "\n"
  print message.to_json
  exit 255
end

require_relative "./Dobject/Dobject.rb"

Dir["#{当前路径}/**/加载.rb"].sort.each do |filename|
  require filename
end
