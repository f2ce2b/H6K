请求参数 = ARGV.delete_at(-1)
处理结果 = []
原始链接 = 请求参数[:链接].clone

请求参数[:链接] = 原始链接.gsub('{当前页}', '1')
res = F访问.请求(**请求参数)
断点调试(binding) unless res[0]
处理结果 << res[1]

其他信息 = res[1].gsub(/.*records:(\d{1,}),pages:(\d{1,}),curpage:(\d{1,})};/, '\1-\2-\3').split('-').map(&:to_i)
断点调试(binding) unless 其他信息.size == 3
断点调试(binding) unless 其他信息.map(&:class).uniq == [Integer]

数据总数, 总页数, 当前页 = 其他信息
断点调试(binding) unless (数据总数/20.0).ceil == 总页数

(2 .. 总页数).each do |当前页|
  请求参数[:链接] = 原始链接.gsub('{当前页}', 当前页.to_s)
  res = F访问.请求(**请求参数)
  断点调试(binding) unless res[0]
  处理结果 << res[1]
end

ARGV.push(处理结果)