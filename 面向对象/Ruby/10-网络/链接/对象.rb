require_relative '../../东方财富/加载.rb'

class Link < Base
  attr_accessor :对象

  def initialize(**params)
    super(**params)
    当前路径 = File.expand_path(File.dirname(__FILE__))
    对象路径 = File.join([当前路径, '对象'])

    迭代目录 = lambda do |路径:, 适用对象:|
      Dir.foreach(路径.join('/')).sort.each do |filename|
        next if ['.', '..'].include?(filename)
        path = [路径, filename].join('/')
        if File.directory?(path)
          迭代目录.call(路径: [路径, filename].flatten, 适用对象: 适用对象)
        elsif File.file?(path) && path.match(/.*\.rb$/)
          ((self.对象||={})[路径[1..-1].join('/')]||={})[filename.gsub(/\d{2}\-(.*).rb/, '\1')] = path
        else
          内容 = File.open(path){|f| f.read}

          ((self.对象||={})[路径[1..-1].join('/')]||={})[filename.gsub(/\d{2}\-/, '')] = case 内容
          when /{.*}/
            array = 内容.gsub(/[^{|}]*\{(.*?)\}[^{|}]*/, '\1间隔符').split('间隔符')
            array.each do |方法名|
              next if ['页码'].include?(方法名)

              if 方法名 =~ /^运行时_.*/
                内容 = 内容.gsub("#{方法名}", 方法名.gsub('运行时_', ''))
                next
              end

              参数值 = 适用对象.send(方法名) rescue 断点调试(binding)
              内容 = 内容.gsub("{#{方法名}}", 参数值)
            end
            JSON.parse(内容) rescue 内容
          else
            JSON.parse(内容) rescue 内容
          end
          
        end
      end
    end

    Dir["#{对象路径}/*"].each do |文件夹|
      数组 = 文件夹.split('/')
      适用对象 = Object.const_get(数组[-1])
      迭代目录.call(路径: [数组[0...-1].join('/'), 数组[-1]], 适用对象: 适用对象)
    end
  end

  def 缓存时效(缓存时效)
    case 缓存时效
    when '月度'
      Time.now.strftime("%Y%m")
    when '半年'
      str = []
      str << Time.now.year
      month = Time.now.month
      str << (month <= 6 ? 6 : 12)
      str << 30
      str.join
    when '报告季度'
      now = Time.now

      # 报告公布具有一定的滞后
      # 例如4季度的报告是在明年1月份的具体哪天发布目前未知
      # 所以滞后1个月使用上季度报告
      case now.month
      when 1
        [now.year-1, '09', '30'].join
      when (2..4)
        [now.year-1, '12', '31'].join
      when (5..7)
        [now.year, '03', '31'].join
      when (8..10)
        [now.year, '06', '30'].join
      when 11, 12
        [now.year, '09', '30'].join
      else
        断点调试(binding)
      end
    when '永久', NilClass, ''
      nil
    else
      断点调试(binding)
    end
  end

  def 访问(关键字:, **args)
    结果 = self.对象.select{|k,v| k.include?(关键字)}
    断点调试(binding) if 结果.keys.size != 1
    存储路径, 详情 = 结果.to_a[0]

    请求参数 = []
    method(__method__).parameters.each do |arg| 
      name = arg[1].to_s
      next if name == 'args'
      请求参数 << [name, binding.local_variable_get(name)]
    end
    请求参数 = 请求参数.to_h.merge!(详情)

    请求参数['盐'] = self.缓存时效(详情['缓存时效'])
    请求参数['缓存文件夹'] = ['.网络缓存', 存储路径].join('/')
    请求参数 = 请求参数.to_h.merge!(args)
    请求参数 = Hash[请求参数.map{|(k,v)| [k.to_sym,v]}]
    
    result = if 详情['处理逻辑']
      self.处理逻辑访问(**(请求参数.merge!({逻辑文件: 详情['处理逻辑']})))
    elsif 详情['链接'].include?('{页码}')
      self.迭代页码访问(**请求参数) 
    else
      res = F访问.请求(**请求参数)
      断点调试(binding) unless res[0]
      res[1]
    end

    if 详情['数据校验']
      断点调试(binding) unless 详情['数据校验'] =~ /\.rb$/
      ARGV.push(result)
      require 详情['数据校验']
      result = ARGV.delete_at(-1)
    end

    [true, result]
  end

  def 处理逻辑访问(逻辑文件:, **args)
    ARGV.push(args)
    require 逻辑文件
    ARGV.delete_at(-1)
  end

  def 迭代页码访问(**args)
    断点调试(binding) unless args[:分页结果].is_a?(Array)
    原始链接 = args[:链接]
    args[:链接] = args[:链接].gsub('{页码}', '1')

    响应结果 = []
    res = F访问.请求(**args)
    断点调试(binding) unless res[0]
    return res if [[], {}].include?(res[1])
    # 不是哈希结构后面无法迭代页码
    断点调试(binding) unless res[1].is_a?(Hash)

    响应结果 << res[1]

    迭代页码值 = lambda do |当前结果:, 键组:|
      当前键 = 键组.delete_at(0)
      当前结果 = 当前结果[当前键]
      return 当前结果 if 键组.size == 0
      迭代页码值.call(当前结果: 当前结果, 键组: 键组)
    end

    页码数量 = 迭代页码值.call(当前结果: res[1], 键组: args[:分页结果].clone)
    断点调试(binding) unless 页码数量.is_a?(Integer)

    (2..页码数量).each do |页码|
      args[:链接] = 原始链接.gsub('{页码}', 页码.to_s)
      res = F访问.请求(**args)
      断点调试(binding) unless res[0]
      断点调试(binding) if [[], {}].include?(res[1])
      断点调试(binding) unless res[1].is_a?(Hash)
      响应结果 << res[1]
    end

    响应结果
  end

  def 缓存(关键字:)
    匹配内容 = self.对象.keys.select{|key| key.include?([nil, 关键字, nil].join('/'))}
    匹配内容.flatten.each do |keyword|
      self.访问(关键字: keyword)
    end
  end

end