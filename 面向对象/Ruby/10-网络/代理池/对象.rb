class ProxyPool < Dobject
  attr_accessor :当前代理

  def initialize(**params)
    current_path = File.expand_path(File.dirname(__FILE__))
    params[:属性] = "#{current_path}/属性"
    params[:方法] = "#{current_path}/方法"
    super(**params)

    res = RestClient.get("#{self.地址}:#{self.端口}")
    raise "代理服务自检失败!" unless res.code == 200
  end
end
