module FangFa
  # api        method   Description       params
  # /          GET     api介绍             None
  # /get       GET     随机获取一个代理      可选参数: ?type=https 过滤支持https的代理
  # /pop       GET     获取并删除一个代理    可选参数: ?type=https 过滤支持https的代理
  # /all       GET     获取所有代理         可选参数: ?type=https 过滤支持https的代理
  # /count     GET     查看代理数量         None
  # /delete    GET     删除代理            ?proxy=host:ip

  def 链接
    return "#{self.地址}:#{self.端口}"
  end

  def 删除代理(地址:)
    host_ip = 地址.split('//')[1]
    res = RestClient.get("#{self.链接}/delete/?proxy=#{host_ip}")
    断点调试(binding) if res.code != 200

    [true, '']
  end
end