module HuoQu
  def 获取所有存储代理
    return [true, '成功'] if (@代理池||=[]).size > 1

    self.更新代理池
  end

  def 挑选可用代理节点
    总数 = @代理池.size
    @代理池.each_with_index do |代理, 下标|
      @代理 = @代理池.delete_at(下标)
      请求参数 = {
        # 'url' => 'www.baidu.com', 
        # 'url' => 'https://datacenter-web.eastmoney.com', 
        'url' => 'https://www.eastmoney.com/', 
        'method' => 'get', 
        'proxy' => @代理, 
        'timeout' => 5, 
        'open_timeout' => 5
      }

      请求参数.delete_if{|k, v| k == 'proxy'} if @代理 == 'LocalHost'
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 测试代理: [#{下标+1}/#{总数}] #{@代理}\n"
      
      begin
        response = RestClient::Request.execute(**Hash[请求参数.map{|(k,v)| [k.to_sym,v]}])  
      rescue => exception
        next if @代理 == 'LocalHost'
        res = self.删除代理(地址: @代理)
        断点调试(binding) unless res[0]
        next
      end

      if response.code == 200
        @代理池 << @代理
        # TODO: 这里的逻辑还是存在问题 但目前没有更好的思路
        break if @代理 != 'LocalHost'
      else
        断点调试(binding)
        next
      end
    end

    # 循环中返回结果会导致后面流程步骤不执行 不理解
    return [true, '成功'] if @代理池.size > 0

    @失败次数||=0
    @失败次数 += 1

    if (@失败次数) > 5
      return [true, '多次失败']
    else
      return [true, '失败']
    end
  end

  def 更新代理池
    res = RestClient.get("#{self.链接}/all/")
    return [true, '失败'] if res.code != 200

    @代理池 = JSON.parse(res.body).map{|proxy| [(proxy['https'] ? 'https:/': 'http:/'), proxy['proxy']].join('/')}
    @代理池 << 'LocalHost'
    @代理池 = @代理池.shuffle
    [true, '成功']
  end

  def 返回节点信息
    self.当前代理 = @代理
    [true, @代理]
  end

  def 返回本地代理
    @代理 = 'LocalHost'
    返回节点信息
  end

end