module TiaoKong
  def 当前域名是否限频
    相关调控配置 = @调控配置.select { |k, v| @域名.include?(k) || k.include?(@域名) }
    if 相关调控配置.values.size == 0
      self.当前代理 = nil
      return [true, "否"] 
    end
    断点调试(binding) if 相关调控配置.values.size != 1
    @相关调控配置 = 相关调控配置.values[0]
    [true, "是"]
  end

  def 当前是否分配代理
    return [true, "否"] unless self.当前代理
    [true, "是"]
  end

  def 分配代理
    res = D代理池.获取代理节点
    断点调试(binding) unless res[0]
    self.当前代理 = res[1]
    res
  end

  def 请求量是否接近阈值
    url = Es.链接_es("/#{self.日志索引名}/_doc/_search")

    请求参数 = {
      'query': {
        'bool': {
          'must': [
            {'match': {'域名': @域名}},
            {'match': {'请求代理': self.当前代理}},
            'range': {
              '请求时间': {
                'gte': 'now-1m',
                # 'gte': 'now-1h',
                'lte': 'now'
              }
            }
          ]
        }
      }
    }

    args = {
      # url: URI.parse(URI.escape(url)).to_s,
      url: CGI.escape(url).gsub('%3A', ':').gsub('%2F', '/'),
      method: 'POST',
      headers: {'content-type' => 'application/json'},
      payload: 请求参数.to_json,
    }

    res = RestClient::Request.execute(**args)
    断点调试(binding) unless [200,201].include?(res.code)
    result = JSON.parse(res.body)
    断点调试(binding) if result['timed_out']
    @限时请求量 = result['hits']['total']['value']
    @频率限制 = @相关调控配置['频率限制']

    # 阈值保持在0.7-0.9的范围内
    随机阈值 = rand()*(0.9-0.7)+0.7
    return [true, '否'] unless ((@限时请求量 / @频率限制.to_f) > 随机阈值)
    print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 接近阈值: #{@限时请求量}/#{@频率限制}\n"
    
    [true, '是']
  end

  def 开始休眠
    return [true, ''] unless self.当前代理 == 'LocalHost' 
    休眠时间 = rand(15..30)
    print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 开始休眠: #{休眠时间}秒...\n"
    sleep 休眠时间
    [true, '']
  end
end
