module TiaoKong

  def 请求频率调控(链接: )
    流程 = {
      '当前域名是否限频': {
        '否': "跳出流程",
        '是': {
          '当前是否分配代理':{
            '否': '分配代理'
          },
          '请求量是否接近阈值': {
            '否': "跳出流程",
            '是': [
              "开始休眠",
              "分配代理",
              "重新循环流程",
            ],
          },
        },
      },
    }
    @域名 = 链接.split('://')[1].split('/')[0]
    @调控配置 ||= File.open(File.expand_path([File.dirname(__FILE__), './调控配置.json'].join('/'))){|file| JSON.parse(file.read)}

    res = L流程.执行流程(流程: 流程, 对象: self)
    断点调试(binding) if (!res[0] && res[1].to_i == 0)
    return [true, ""] if res[1] == 200
    return res
  end
end
