module RiZhi

  def 创建日志(id:, 文档:)
    @运行参数 ||= JSON.parse(ARGV[0])
    文档 = Hash[文档.map{|(k,v)| [k.to_sym,v]}]
    文档[:身份标识] = self.身份标识

    url = CGI.escape(Es.链接_es("/#{self.日志索引名}/_doc/#{id}")).gsub('%3A', ':').gsub('%2F', '/')
    args = {
      # url: URI.parse(URI.escape(Es.链接_es("/#{self.日志索引名}/_doc/#{id}"))).to_s,
      url: url,
      method: 'POST',
      headers: {'content-type' => 'application/json'},
      payload: 文档.to_json
    }

    res = RestClient::Request.execute(**args)
    断点调试(binding) unless [200,201].include?(res.code)
    [true, id]
  end

  def 更新日志(id:, 文档:)
    @运行参数 ||= JSON.parse(ARGV[0])
    args = {
      # url: URI.parse(URI.escape(Es.链接_es("/#{self.日志索引名}/_update/#{id}"))).to_s,
      url: CGI.escape(Es.链接_es("/#{self.日志索引名}/_update/#{id}")).gsub('%3A', ':').gsub('%2F', '/'),
      method: 'POST',
      headers: {'content-type' => 'application/json'},
      payload: {"doc": 文档}.to_json,
    }

    res = RestClient::Request.execute(**args)
    断点调试(binding) unless [200,201].include?(res.code)
    [true, id]
  end

end