module QingQiu
  DefaultPrintFormat = lambda do |参数|
    输出 = lambda do |内容1, 内容2| 
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] #{内容1}: #{内容2}\n"
    end

    输出.call('当前代理', 参数[:代理]) if 参数[:代理]
    输出.call('开始请求', 参数[:链接])
    输出.call('请求内容', 参数[:请求体]) if 参数[:请求体]
  end

  def 请求(链接:, 请求方法: "Get", 请求超时: 15, 请求头: nil, 请求体: nil, 输出日志: DefaultPrintFormat, 重试次数: 10, cookies: nil, 请求标识: SecureRandom.uuid, 请求模式: '缓存优先', **args)

    断点调试(binding, 备注: '链接内存在未处理{参数}') if 链接 =~ /.*\{.*\}.*/

    unless 链接 =~ /.*127\.0\.0\.1.*/
      case 请求模式
      when '缓存优先'
        请求参数 = []
        method(__method__).parameters.each do |arg| 
          name = arg[1].to_s
          next if name == 'args'
          请求参数 << [name, binding.local_variable_get(name)]
        end
        请求参数 = 请求参数.to_h.merge!(args)

        return Rc.缓存(**Hash[请求参数.map{|(k,v)| [k.to_sym,v]}])
      end

      self.请求频率调控(链接: 链接)
    end
    断点调试(binding) if self.当前代理 == '成功'

    输出日志.call(**{链接: 链接, 请求体: 请求体, 请求方法: 请求方法, 代理: self.当前代理})

    if 链接 =~ /\p{Han}+/
      # url = URI.parse(URI.escape(链接)).to_s
      url = CGI.escape(链接).gsub('%3A', ':').gsub('%2F', '/')
    else
      url = 链接
    end

    请求参数 = {
      'url' => url, 
      'method' => 请求方法, 
      'headers' => 请求头, 
      'cookies' => nil, 
      'proxy' => self.当前代理,
      'payload' => (请求体.is_a?(Hash) ? 请求体.to_json : 请求体), # Rest-client 本身不支持 JSON, 因此在将负载传递给 Rest-client 之前将其序列化为字符串.
      'timeout' => 请求超时, 
      'open_timeout' => 请求超时
    }.delete_if{|k,v| v.nil?}

    请求参数.delete_if{|k, v| k == 'proxy'} if self.当前代理 == 'LocalHost'

    begin
      请求时间 = Time.now.strftime('%Y-%m-%dT%H:%M:%S.%L%z')
      文档 = {
        '域名': 链接.split('://')[1].split('/')[0],
        '链接': 链接,
        '请求方法': 请求方法.upcase,
        '请求时间': 请求时间,
        '请求代理': self.当前代理,
      }
      res_ = self.创建日志(id: 请求标识, 文档: 文档)
      断点调试(binding) unless res_[0]
      response = RestClient::Request.execute(**Hash[请求参数.map{|(k,v)| [k.to_sym,v]}])
    rescue Errno::ECONNRESET, SocketError, RestClient::Exceptions::OpenTimeout, RestClient::Exceptions::ReadTimeout, Net::HTTPFatalError, RestClient::ServiceUnavailable, RestClient::BadGateway, Net::HTTPServerException, RestClient::ServerBrokeConnection => e
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 请求失败: 连接异常(#{e.class})\n"
    rescue RestClient::NotFound
      response = [false, '返回404状态', {响应状态: 404}]
    rescue URI::InvalidURIError
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 请求失败: URL编码错误\n"
      # 链接 = URI.parse(URI.escape(url)).to_s
      链接 = CGI.escape(url).gsub('%3A', ':').gsub('%2F', '/')
    rescue => e
      断点调试(binding)
      response = [false, e.to_s]
    ensure
      响应时间 = Time.now.strftime('%Y-%m-%dT%H:%M:%S.%L%z')
      响应时间.to_i - 请求时间.to_i
      更新文档 = {
        '响应时间': 响应时间,
        '耗时(毫秒数)': ((Time.parse(响应时间).to_f - Time.parse(请求时间).to_f) * 1_000).to_i,
      }
      res = []
      case response
      when RestClient::Response
        flag = [200, 201].include?(response.code)
        res = [flag, (JSON.parse(response.body) rescue response.body)]
      when Array
        res = response
      when NilClass
        self.当前代理 = nil

        请求参数 = []
        method(__method__).parameters.each do |arg| 
          name = arg[1].to_s
          next if name == 'args'
          请求参数 << [name, binding.local_variable_get(name)]
        end
        请求参数 = 请求参数.to_h.merge!(args)

        print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 请求失败: [#{请求参数['重试次数']}]再次尝试......\n"

        请求参数['重试次数'] -= 1
        unless 请求参数['重试次数'] > 0
          print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 请求失败: 重试次数过多......\n"
          断点调试(binding) 
        end
        return self.send(__method__, **Hash[请求参数.map{|(k,v)| [k.to_sym,v]}])
      else
        断点调试(binding)
      end
    end

    if res[0]
      更新文档['响应状态'] = response.code.to_i
    else
      更新文档['响应状态'] = res[2][:响应状态]
    end

    res_ = self.更新日志(文档: 更新文档, id: 请求标识)
    断点调试(binding) unless res_[0]
    res
  end
end