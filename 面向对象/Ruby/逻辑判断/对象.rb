class TrueORFalse < Base

  def 运算(数值A, 数学符号, 数值B)
    return case 数学符号.to_s
    when '等于'
      数值A == 数值B
    when '包含'
      !!(数值A =~ Regexp.new(数值B))
    else
      断点调试(binding)
    end
  end

  def 校验(条件:)
    断点调试(binding) unless 条件.size == 3
    属性组, 数学符号, 对比值组 = 条件
    结果 = {}
    [对比值组].flatten.each do |对比值|
      [属性组].flatten.each do |属性|
        属性值 = self.send(属性)
        res = self.运算(属性值, 数学符号, 对比值)

        说明值 = case 对比值 
        when '' 
          "\'\'"
        when nil
          '空'
        else 
          对比值
        end

        (结果[res]||=[]) << {
          '属性' => 属性,
          '当前值' => (属性值||'空'),
          '符号' => "#{res ? '': '不'}#{数学符号}",
          '对比值' => 说明值
        }

      end
    end    
    # 结果 = 结果.map{|k,v| [k,v.flatten]}.to_h

    [结果[true]!=nil, 结果]
  end

end

class Logic < TrueORFalse

  def 判断(条件:)
    结果 = {}

    条件.each do |逻辑, 构成|
      断点调试(binding) unless 构成.is_a?(Array)
      case 逻辑.to_s
      when '或'
        结果_逻辑 = {}
        构成.each do |数组|
          res = self.校验(条件: 数组)
          res[1].each do |是否, 说明|
            结果_逻辑[是否] = [(结果_逻辑[是否]||=[]), 说明].flatten
          end
        end

        (结果[结果_逻辑[true]!=nil]||=[]) << Hash[逻辑.to_s, 结果_逻辑]
      else
        断点调试(binding)
      end
    end

    res = {}
    结果.values.flatten.map(&:values).flatten.map{|k| k.map{|k,v| [k,v].flatten}}[0].each do |数组|
      res[数组[0]] = [(res[数组[0]]||[]), 数组[1..-1]].flatten
    end

    [结果[false]==nil, res, 结果]
  end

  def method_missing(sym, *args)
    return J结果集.find_by(键: sym)
  end
end