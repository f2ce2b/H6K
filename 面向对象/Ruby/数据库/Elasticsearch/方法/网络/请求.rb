module WangLuo
  # RestClient在遇到404返回状态后直接报错 但实际是有正常返回内容
  def GET请求(链接:)
    print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 开始请求: #{链接}\n"
    # url = URI(CGI.escape(链接).gsub('%3A', ':').gsub('%2F', '/'))
    # response = RestClient.get("#{url.host}:#{url.port}")
    
    begin
      response = RestClient.get(CGI.escape(链接).gsub('%3A', ':').gsub('%2F', '/'))
    rescue RestClient::NotFound
      response = OpenStruct.new(code: 404, body: 'RestClient::NotFound')
    rescue => exception
      断点调试(binding)
    ensure
      return [[200, 201].include?(response.code.to_i), (JSON.parse(response.body) rescue response.body), {响应状态: response.code.to_i} ]
    end
  end

  def 请求(链接:, 请求方法: "Get", 请求超时: 15, 请求头: nil, 请求体: nil, 输出日志: nil, 请求模式: '直接请求')
    请求参数 = method(__method__).parameters.map do |arg| 
      name = arg[1].to_s
      [name, binding.local_variable_get(name)]
    end.to_h.delete_if{|k,v| v.nil?}

    F访问.请求(**Hash[请求参数.map{|(k,v)| [k.to_sym,v]}])
  end
end