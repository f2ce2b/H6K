module Cat

  def 查看_索引
    res = self.请求(链接: self.链接_es("/_cat/indices?format=json"))
    indices_all = res[1].delete_if{|hash| hash['index'] =~ /^\..*/ }

    字典 = {
      "index" => "名称",
      "docs.count" => "文档数量",
      "store.size" => "存储空间",
    }

    result = []
    indices_all.each do |index|
      res = {}
      字典.each do |键,值|
        res[值] = case 值
        when '存储空间'
          index[键].upcase
        else
          index[键]
        end
      end
      result << res
    end

    result
  end

end