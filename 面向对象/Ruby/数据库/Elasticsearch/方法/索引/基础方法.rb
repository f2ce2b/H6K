module SuoYin
  def 链接_es(路由=[])
    路由 = ['', 路由].flatten.join('/').gsub('//', '/')
    "#{self.地址}:#{self.端口}#{路由}"
  end

  def 新建文档(索引名:, id: SecureRandom.uuid, 内容:)
    res = self.请求(链接: self.链接_es("/#{索引名}/_doc/#{id}"), 请求方法: "POST", 请求头: {'content-type' => 'application/json'}, 请求体: 内容)
    断点调试(binding) unless res[0]
    res
  end

  def 全部索引
    return @indices_all if @indices_all
    res = self.请求(链接: self.链接_es("/_cat/indices?format=json"))
    @indices_all = res[1].map{|hash| hash['index']}
    @indices_all.delete_if{|item| item =~ /^\..*/}
  end

  def 全部索引别名
    return @aliases_all if @aliases_all
    res = self.请求(链接: self.链接_es("/_cat/aliases?format=json"))
    @aliases_all = res[1].map{|hash| hash['alias']}
    @aliases_all.delete_if{|item| item =~ /^\..*/}
  end

  def 删除索引(名称: )
    res = self.请求(链接: self.链接_es(名称), 请求方法: "DELETE")
    断点调试(binding) unless res[0]
    res
  end

  def 删除匹配文档(名称:, 匹配条件: {"match_all": {}})
    res = self.请求(链接: self.链接_es("/#{名称}/_delete_by_query"), 请求方法: "POST", 请求头: {'content-type' => 'application/json'}, 请求体: {"query": 匹配条件})
    断点调试(binding) unless res[0]
    res
  end

  def 重新索引(旧名称:,新名称:)
    请求体 = {
      "source": {
        "index": 旧名称
      },
      "dest": {
        "index": 新名称
      }
    }

    res = self.请求(链接: self.链接_es("_reindex"), 请求方法: "POST", 请求头: {'content-type' => 'application/json'}, 请求体: 请求体)
    断点调试(binding) unless res[0]
    res
  end

  def 所有文档ID(名称:, 请求数量: 10000)
    res = self.请求(链接: self.链接_es("/#{名称}/_search?stored_fields=&size=#{请求数量}"))
    断点调试(binding) unless res[0]
    hits = res[1]['hits']
    断点调试(binding) if hits['total']['value'] > 请求数量
    [true, hits['hits'].map{|item| item['_id']}]
  end
end