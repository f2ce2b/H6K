module SuoYin
  def 生成索引定义及文档(定义:, 目标对象:, 说明:)
    基本信息 = {
      "映射" => {},
      "文档" => {},
    }
    
    目标对象.each do |id, 对象|
      对象文档 = {}
      定义.each do |键, 字段定义|
        unless 对象.keys.map(&:to_s) == 定义.keys.map(&:to_s)
          断点调试(binding) unless (对象.keys & 定义.keys).size == 定义.keys.size
        end
        属性值 = 对象[键] || 对象[键.to_s]|| 对象[键.to_sym] || 断点调试(binding, "#{说明}中字段[#{键}]值为空")
        对象文档["#{字段定义['字段名']||键}"] = 属性值
      end
      对象文档['MD5校验'] = Digest::MD5.hexdigest(JSON.pretty_generate(对象文档))
      基本信息['文档'][id] = 对象文档
    end
    

    字典 = {
      'type': '类型',
      'format': '格式',
    }
    字典 = 字典.map{|k, v| [[v.to_sym,k], [v.to_s,k]].to_h}.reduce({}, :merge)

    mapping = {}
    定义.each do |键, 字段定义|
      map = mapping["#{字段定义['字段名']||键}"] = {}
      字典.each do |键,值|
        next unless 字段定义[键]
        map[值] = 字段定义[键]
      end
      断点调试(binding) if map.values.include?(nil)
    end

    mapping['MD5校验'] = {'type': "keyword"}
    基本信息['映射'] = {"properties": mapping}
    return 基本信息
  end
end