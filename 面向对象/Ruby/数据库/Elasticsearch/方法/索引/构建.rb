module SuoYin

  def 自动构建(映射:, 文档:, 主索引名:)
    method(__method__).parameters.map{|arg| self.instance_variable_set("@#{arg[1]}", binding.local_variable_get(arg[1])) }
    @index_md5 = Digest::MD5.hexdigest(JSON.pretty_generate({'主索引名': @主索引名}.merge!(@映射)))
    @子索引名 = [@主索引名, @index_md5].join('_')

    @main_index_url = self.链接_es(@主索引名)
    @node_index_url = self.链接_es(@子索引名)
    
    流程 = {
      '构建索引': {
        '检查索引是否存在': {
          '存在': {
            '子索引是否存在': {
              '是': [
                '文档检验及更新',
                '跳出流程'
              ],
              '否': [
                '删除所有子索引',
                '创建子索引',
                '子索引关联主索引',                
              ]
            },
          },
          '不存在': [
            '创建子索引',
            '子索引关联主索引',
          ],
        },
      },
      '定义索引': nil,
      '创建文档': nil
    }

    res = L流程.执行流程(流程: 流程, 对象: self)
    断点调试(binding) if (!res[0] && res[1].to_i == 0)
    return [true, ''] if res[1] == 200
    return res
  end
end
