module SuoYin
  def 检查索引是否存在
    res = self.GET请求(链接: @main_index_url)
    unless res[0]
      断点调试(binding) unless res[2][:响应状态] == 404
    end

    @main_index_detail = res[1]
    [true, (res[0] ? '存在' : '不存在')]
  end
  
  def 子索引是否存在
    res = self.全部索引
    # match_index = res.select{|index| index.include?(@index_md5) }
    match_index = res.select{|index| index == @子索引名 }
    return [true, (match_index.size>0 ? '是' : '否')]
  end

  def 删除所有子索引
    index_detail = @main_index_detail.clone
    index_detail.delete_if{|k, v| k == @子索引名}
    index_detail.each do |name, value|
      res = self.请求(链接: self.链接_es(name), 请求方法: "DELETE")
      断点调试(binding) unless res[0]
    end
    [true, '']
  end

  def 创建子索引
    res = self.请求(链接: @node_index_url, 请求方法: "PUT")
    断点调试(binding) unless res[0]
    res
  end

  def 子索引关联主索引
    self.请求(链接: "#{@node_index_url}/_alias/#{@主索引名}", 请求方法: "PUT")
  end

  def 定义索引
    res = self.请求(链接: "#{@node_index_url}/_mapping", 请求方法: "PUT", 请求头: {'content-type' => 'application/json'}, 请求体: @映射)
    断点调试(binding) unless res[0]
    res
  end

  def 创建文档
    @文档.each do |id, 内容|
      res = self.新建文档(索引名: @子索引名, id:id, 内容:内容)
      断点调试(binding) unless res[0]
    end
    [true, '']
  end

  def 内容是否完全相同
    res = self.全部索引
    match_index = res.select{|index| index.include?(@doc_md5) }
    return [true, (match_index.size>0 ? '是' : '否')]
  end

  def 重新进行索引
    # 重新索引流程
    # # 重建索引默认并不会复制映射过去,只是简单的迁移了文档, 如果未对新索引定义将动态映射
    # '创建子索引',
    # '定义索引',
    # '重新进行索引',
    # '子索引关联主索引',
    # '删除旧索引',
    # '跳出流程'
    self.重新索引(旧名称: @node_index_old_name, 新名称: @子索引名)
    [true, '']
  end

  def 删除旧索引
    self.删除索引(名称: @node_index_old_name)
    [true, '']
  end

  def 文档检验及更新
    @文档.each do |id, 文档|
      res = self.GET请求(链接: "#{@node_index_url}/_doc/#{id}")
      unless res[0]
        断点调试(binding) unless res[2][:响应状态] == 404
      end

      if res[1]['found']
        _source = res[1]['_source']
        next if _source['MD5校验'] == 文档['MD5校验']
        res = self.删除匹配文档(名称: @子索引名, 匹配条件: {"match": {"_id": id}})
        断点调试(binding) unless res[0]
        断点调试(binding) if res[1]['deleted'] == 0
      end

      res = self.新建文档(索引名: @子索引名, id:id, 内容:文档)
      断点调试(binding) unless res[0]
      断点调试(binding) unless res[1]['result'] == 'created'
    end
    [true, '']
  end

end
