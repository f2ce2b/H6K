class Statistic < Base
  def 描述性统计(数组:, 结果模式: '详细')
    断点调试(binding) unless 数组.is_a?(Array)
    断点调试(binding) unless (数组.map(&:class).uniq - [Float, Integer]) == []

    数组.sort!

    统计结构 = {
      描述性统计: {
        说明: [
          '描述性统计是指对数据进行简单的描述,以了解数据的基本情况.'
        ],
        集中趋势: {
          说明: [
            '集中趋势是指一组数据中所有数据的中心位置,反映数据的集中程度'
          ],
          最大值: {
            数值: 数组.max
          },
          最小值: {
            数值: 数组.min
          },
          平均值: {
            数值: (数组.sum / 数组.size)
          },
          中位数: {
            说明: [
              '指将一组数据按照大小顺序排列后,居于中间位置的数据',
              '中位数不受极端值的影响,因此在数据中存在极端值的情况下',
              '中位数往往比平均值更能反映数据的集中趋势.'
            ],
            数值: self.中位数(数组: 数组),
          },
          中位数偏差: {
            说明: [
              '平均值与中位数之差',
              '中位数偏差可以用来衡量数据分布的偏斜程度',
            ],
            数值: (数组.sum / 数组.size) - self.中位数(数组: 数组),
          },
        },
        离散程度: {
          说明: [
            '离散程度是指一组数据中各个数据值之间的差异程度',
            '反映数据的分散程度'
          ],
          极差: {
            说明: [
              '是指一组数据中最大值与最小值之差',
              '极差是离散程度最简单的指标',
              '但它容易受极端值的影响'
            ],
            数值: (数组.max - 数组.min),
          },
          平均差: {
            说明: [
              '是指一组数据中所有数据与平均值的差值的平均值',
              '平均差不受极端值的影响',
              '平均差越大，表示数据分布越分散。平均差越小，表示数据分布越集中。',
            ],
            数值: self.平均差(数组: 数组),
          },
          方差: {
            说明: [
              '是指一组数据中所有数据与平均值的差值的平方的平均值',
              '方差是离散程度最常用的指标',
              '但它不具有可比性'
            ],
            数值: self.方差(数组: 数组),
          },
          标准差: {
            说明: [
              '方差的算术平方根',
              '标准差具有可比性',
              '是离散程度最常用的指标'
            ],
            数值: self.标准差(数组: 数组),
          },
          四分位差: {
            说明: [
              '是指一组数据中四分位数之间的差值',
              '四分位差不受极端值的影响',
              '但它比标准差更难计算'
            ],
            数值: self.四分位差(数组: 数组),
          },
        },
        分布形状: {
          偏度: {
            说明: [
              '是指数据分布曲线的对称程度',
              '偏度可以用来衡量数据分布的形状',
            ],
            数值: self.偏度(数组: 数组),
          },
          峰度: {
            说明: [
              '是数据分布曲线的尖锐程度',
              '峰度可以用来衡量数据分布的形状',
            ],
            数值: self.峰度(数组: 数组),
          },
        },
      }
    }
    self.处理结果(数组: 数组, 统计结构: 统计结构)
  end

  def 处理结果(数组:, 统计结构:)

    结构补充 = lambda do |结构:|
      case 结构
      when Hash
        结构.each do |k,v|
          结构补充.call(结构: v)
        end

        # 结构['中文'] = self.数字转中文(结构['数值']) if 结构['数值'] && 结构['数值'] > 1000
        结构['数值'] = [结构['数值'], self.数字转中文(结构['数值'])] if 结构['数值'] && 结构['数值'] > 9999
      end
      结构
    end
    详细结构 = 结构补充.call(结构: JSON.parse(统计结构.to_json))

    结构精简 = lambda do |结构:, 上级结构: nil, 上级键: nil |
      case 结构
      when Hash
        结构.delete_if{|k,v| ['说明'].include?(k)}
        结构['数值'] = 结构['数值'][0] if 结构['数值'] && 结构['数值'].is_a?(Array)
        结构.each do |k,v|
          结构精简.call(结构: v, 上级结构: 结构, 上级键: k)
        end
        上级结构[上级键] =  结构.values[0] if (结构.keys == ['数值']) && 上级结构 && 上级键
      end
      结构
    end
    精简结构 = 结构精简.call(结构: JSON.parse(统计结构.to_json))

    结构压缩 = lambda do |结构:, 结果: {}, 上级键: nil |
      case 结构
      when Hash
        结构.each do |k,v|
          结构压缩.call(结构: v, 结果: 结果, 上级键: k)
        end
      when Integer, Float
        结果[上级键] = 结构
      else
        断点调试(binding)
      end
      结果
    end
    精简结构 = 结构压缩.call(结构: 精简结构)

    {
      '精简结构' => 精简结构,
      '详细结构' => 详细结构,
      '样本数量' => 数组.size,
      '样本数据' => 数组,
    }
  end

  def 数字转中文(数字)
    整数,小数 = 数字.to_s.split('.')

    遍历数组 = lambda do |数组:, 整数: true, 结果:[]| 

      结果 << case 数组[0]
        when "0" then "零"
        when "1" then "一"
        when "2" then "二"
        when "3" then "三"
        when "4" then "四"
        when "5" then "五"
        when "6" then "六"
        when "7" then "七"
        when "8" then "八"
        when "9" then "九"
      end

      # 断点调试(binding) if "0987206095".split('') == 数组
      结果.delete_at(-1) if 数组[0] == '0' && 数组.size%4 == 1
      结果 << ['', '十', '百', '千'][(数组.size)%4-1] if 整数 && 数组[0] != '0'
      结果 << ['', '万', '亿'][数组.size/4] if 整数 && 数组.size%4 == 1

      数组.delete_at(0)
      return 遍历数组.call(数组: 数组, 结果: 结果, 整数: 整数) if 数组.size != 0
      结果.join
    end

    中文 = [遍历数组.call(数组: 整数.split('').clone), '点', 遍历数组.call(数组: (小数||'').split('').clone, 整数: false)].join    

    中文.gsub(/点$/, '')
  end

  def 方差(数组:)
    m = 数组.inject(:+) / 数组.size
    s = 数组.map { |i| (i - m) ** 2 }.inject(:+).to_f / 数组.size
  end

  def 中位数(数组:)
    数组.sort!
    n = 数组.size
    if n % 2 == 1
      return 数组[n / 2]
    else
      return (数组[n / 2] + 数组[n / 2 - 1]) / 2
    end
  end

  def 平均差(数组:)
    Math.sqrt(数组.sum { |element| (element - (数组.sum / 数组.size))**2 } / 数组.size)
  end

  def 标准差(数组:)
    Math.sqrt(数组.sum { |element| (element - (数组.sum / 数组.size))**2 } / 数组.size)
  end

  def 四分位差(数组:)
    # 将数组排序
    数组.sort!

    # 获取第25%和第75%位置的数据
    q1 = 数组[(数组.size * 0.25).floor]
    q3 = 数组[(数组.size * 0.75).floor]

    q3 - q1
  end

  def 偏度(数组:)
    sum = 0
    数组.each do |element|
      sum += (element - (数组.sum / 数组.size))**3
    end
    
    (sum / 数组.size) / ((数组.sum / 数组.size)**3)
  end

  def 峰度(数组:)
    sum = 0
    数组.each do |element|
      sum += (element - (数组.sum / 数组.size))**4
    end
    (sum / 数组.size) / ((数组.sum / 数组.size)**4)
  end

end
