class FundEastmoney < Base
  attr_accessor :默认请求参数
  attr_accessor :代码

  def initialize(**params)
    super(**params)
    self.默认请求参数 ||= {}
    self.代码 ||= self.目标目录.split('/')[-1]
  end

  def 网络请求(链接:, 请求方法: "Get", 请求超时: nil, 请求头: nil, 请求体: nil, 重试次数: nil, 输出日志: nil, 盐: self.缓存标志, **args)
    # 所有请求交由链接对象处理
    断点调试(binding)

    请求参数 = []
    method(__method__).parameters.each do |arg| 
      name = arg[1].to_s
      next if name == 'args'
      请求参数 << [name, binding.local_variable_get(name)]
    end
    请求参数 = 请求参数.to_h.merge!(args)
    请求参数.merge!(self.默认请求参数).delete_if{|k,v| v.nil?}

    F访问.请求(**Hash[请求参数.map{|(k,v)| [k.to_sym,v]}])
  end

  def 缓存标志
    Time.now.strftime("%Y%m")
  end

  def 报告日期
    now = Time.now

    # 报告公布具有一定的滞后 
    # 例如4季度的报告是在明年1月份的具体哪天发布目前未知
    # 所以滞后1个月使用上季度报告
    case now.month
    when 1
      [now.year-1, '09', '30'].join('-')
    when (2..4)
      [now.year-1, '12', '31'].join('-')
    when (5..7)
      [now.year, '03', '31'].join('-')
    when (8..10)
      [now.year, '06', '30'].join('-')
    when 11, 12
      [now.year, '09', '30'].join('-')
    end
  end

  def 代码_纯数字
    self.代码.split('.')[0]
  end

end
