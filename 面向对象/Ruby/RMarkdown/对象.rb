class RMarkdown < Dobject
  def initialize(**params)
    current_path = File.expand_path(File.dirname(__FILE__))
    # params[:属性] = "#{current_path}/属性"
    # params[:方法] = "#{current_path}/方法"
    super(**params)
  end

  def 生成文档(模板:, 标题: nil)
    模板路径 = []
    current_path = File.expand_path(File.dirname(__FILE__))

    模板路径 << File.join([current_path, '文档模板', '通用内容'])
    模板路径 << File.join([current_path, '文档模板', 模板])

    内容 = []

    加载文档 = lambda do |文件夹, 路径=[], 层级=1|
      Dir.foreach(文件夹).sort.each do |filename|
        next if ['.', '..'].include?(filename)
        path = File.join([路径, filename])
        title = [层级.times.map{'#'}.join, filename.gsub(/\d{2}\-|\.rmd/, '')].join(' ')

        if File.file?(path)
          next unless /.rmd$/ =~ filename
          内容 << title if 层级 != 1
          if 标题 && /头部/ =~ filename
            文件内容 = File.open(path){|file| file.read()}
            文件内容 = 文件内容.gsub(/(.*)(title:.*)(.*)/, '\1{标题}\3').gsub('{标题}', "title: '#{标题}'\n")
            内容 << 文件内容
          else
            内容 << File.open(path){|file| file.read()}
          end
        elsif File.directory?(path)
          内容 << title
          加载文档.call(path, [路径, filename].flatten, 层级+1)
        end

      end
    end

    模板路径.each do |路径|
      断点调试(binding) unless File::directory?(路径)
      加载文档.call(路径, [路径])
    end

    写入文件 = File.join([self.目标目录, "#{模板}.rmd"])
    File.open(写入文件, 'w'){|file| file.write(内容.join("\n\n"))}

    [true, 写入文件]
  end

  def 生成参数(模板:, 数据:, rmd路径:)
    模板路径 = File.join([File.expand_path(File.dirname(__FILE__)), '文档模板', 模板])
    return [false, "#{模板} 模板不存在"] unless File::directory?(模板路径)

    参数 = {
      "绘制" => {},
      "参数" => {
        '文档转换' => {
          "路径" => rmd路径,
          "输出格式" => ["html_document"]
        }
      }
    }

    ARGV.push({
      '数据' => 数据,
      '参数' => 参数,
    })

    加载文档 = lambda do |文件夹, 路径=[]|
      Dir.foreach(文件夹).sort.each do |filename|
        next if ['.', '..'].include?(filename)
        path = File.join([路径, filename])

        if File.file?(path)
          next unless /.rb$/ =~ filename
          断点调试(binding) unless(load path)
        elsif File.directory?(path)
          加载文档.call(path, [路径, filename].flatten)
        end

      end
    end

    加载文档.call(模板路径, [模板路径])
    
    ARGV[-1]['参数']['绘制'].each do |键, 值|
      断点调试(binding) if (值[:数据格式] || 值['数据格式']).nil?
    end

    参数文件 = File.join([self.目标目录, "#{模板}.json"])
    File.open(参数文件, 'w'){|file| file.write(ARGV[-1]['参数'].to_json)}
    
    ARGV.delete_at(-1)
    [true, 参数文件]
  end

  def 渲染(模板:, 数据:, 标题: nil)
    res =self.生成文档(模板: 模板, 标题: (标题 || 模板))
    断点调试(binding) unless res[0]
    rmd路径 = res[1]

    res =self.生成参数(模板: 模板, 数据: 数据, rmd路径: rmd路径)
    断点调试(binding) unless res[0]
    参数路径 = res[1]

    current_path = File.expand_path(File.dirname(__FILE__))
    执行路径 = File.join([current_path, '../', '../', '../', '面向对象', 'R'])
    output = `cd #{执行路径}; Rscript R.r '{"参数文件": "#{参数路径}", "立即绘制": true}'`
    断点调试(binding) unless $?.exitstatus == 0

    File.delete(rmd路径)
    File.delete(参数路径)

    [true, '']
  end

  def 动态生成文档(渲染配置)
    断点调试(binding) unless 渲染配置
    断点调试(binding) unless 渲染配置['绘制']
    绘制 = 渲染配置['绘制']

    解析标题组 = lambda do |标题:, 标题组:, 结果: {}|
      键 = 标题组.delete_at(0)

      if 标题组.size == 0
        结果[键] = [(结果[键]||=[]), [绘制[标题]].flatten.map{|i| Digest::MD5.hexdigest(i.to_json)}].flatten
        return 
      end

      解析标题组.call(标题: 标题, 标题组: 标题组, 结果: (结果[键] ||= {}))
    end

    大纲 = {}
    绘制.keys.each do |标题|
      解析标题组.call(标题: 标题, 标题组: 标题.split('/'), 结果: 大纲)
    end
    
    文档内容 = []
    当前地址 = File.expand_path(File.dirname(__FILE__))
    Dir[File.join([当前地址, '文档模板', '通用内容', '/*'])].sort.each do |path|
      文档内容 << File.open(path){|file| file.read()}
    end

    遍历大纲 = lambda do |结构:, 层级: 1|
      case 结构
      when Hash
        结构.each do |键,值|
          文档内容 << [层级.times.map{'#'}.join, 键].join(' ')
          遍历大纲.call(结构: 值, 层级: (层级+1))
        end
      when Array
        结构.each do |标识|
          文档内容 << "```{r, echo=FALSE, warning=FALSE, out.width='100%', out.height='888'}"
          文档内容 << "绘制('#{标识}')"
          文档内容 << '```'
        end
      else
        断点调试(binding)
      end
    end

    遍历大纲.call(结构: 大纲)

    文档内容 = 文档内容.join("\n")
    文档内容 = 文档内容.gsub(/(.*)(title:.*)(.*)/, '\1{标题}\3').gsub('{标题}', "title: '#{渲染配置['标题'] || 渲染配置['文件名']}'")

    写入文件 = File.join([self.目标目录, "#{渲染配置['文件名']}.rmd"])
    File.open(写入文件, 'w'){|file| file.write(文档内容)}
    [true, 写入文件]
  end

  def 动态生成参数(渲染配置)
    参数 = {
      "绘制" => 渲染配置['绘制'].values.flatten.map{|i| [Digest::MD5.hexdigest(i.to_json), i]}.to_h,
      "参数" => {
        '文档转换' => {
          "路径" => 渲染配置['rmd路径'],
          "输出格式" => ["html_document"]
        }
      }
    }

    参数文件 = File.join([self.目标目录, "#{渲染配置['文件名']}.json"])
    File.open(参数文件, 'w'){|file| file.write(参数.to_json)}
    [true, 参数文件]
  end

  def 动态渲染(渲染配置)
    渲染配置 = JSON.parse(渲染配置.to_json)
    res =self.动态生成文档(渲染配置)
    断点调试(binding) unless res[0]
    渲染配置['rmd路径'] = res[1]

    res =self.动态生成参数(渲染配置)
    断点调试(binding) unless res[0]
    参数路径 = res[1]

    current_path = File.expand_path(File.dirname(__FILE__))
    执行路径 = File.join([current_path, '../', '../', '../', '面向对象', 'R'])
    output = `cd #{执行路径}; Rscript R.r '{"参数文件": "#{参数路径}", "立即绘制": true}'`
    断点调试(binding) unless $?.exitstatus == 0

    File.delete(渲染配置['rmd路径'])
    File.delete(参数路径)

    [true, '']
  end
end
