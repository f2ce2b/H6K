module XuanRan

  # 简介-->|公司名称|东莞勤上光电股份有限公司;
  # 简介-->|A股代码|002638.SZ;
  # 简介-->|A股简称|勤上股份
  # 简介-->|证券类别|深交所主板A股
  # 简介-->|上市交易所|深圳证券交易所
  # 简介-->|&emsp;|所属证监会行业
  # 所属证监会行业-->|&emsp;|1.制造业
  # 所属证监会行业-->|&emsp;|2.电气机械和器材制造业
  # 简介-->|公司网址|www.kingsun-china.com
  # 
  
  def 图表_树状图_校验(对象: , **args)
    return [false, '对象必须为哈希类型'] unless 对象.is_a?(Hash)
    [true, '']
  end

  def 图表_树状图(标题:, 对象:, 树状图方向: '左->右', **args)
    可用方向 = {
      '上->下' => 'TB',
      '下->上' => 'BT',
      '左->右' => 'LR',
      '右->左' => 'RL',
    }
    结果 = ['```mermaid']
    结果 << "graph #{可用方向[树状图方向]}"

    参数值处理 = lambda do |当前标题:, 键:, 值:|
      case 值
      when Hash
        结果 << ["#{当前标题}-->", '&emsp;', 键].join('|')
        值.each do |key, value|
          参数值处理.call(当前标题: 键, 键: key, 值: value)
        end
      when Array
        结果 << ["#{当前标题}-->", '&emsp;', 键].join('|')
        值.each do |item|
          结果 << ["#{键}-->", '&emsp;', "#{值.index(item)+1}.#{item}"].join('|')
        end
      else
        结果 << ["#{当前标题}-->", 键, 值].join('|')
      end
    end

    对象.each do |键, 值|
      参数值处理.call(当前标题: 标题, 键: 键, 值: 值)
    end

    结果 << '```'
    [true, 结果.join("\n")]
  end
end