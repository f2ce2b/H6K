module XuanRan
  def 表格_横向(对象:, **args)
    结果 = []

    表头 = args[:表头] || 对象.map{|item| item.keys}.flatten.uniq
    if 表头.size == 1
      结果 << "#{表头[0]}|"
      结果 << "-|"
    else
      结果 << 表头.join('|')
      结果 << 表头.map{|item|'-'}.join('|')
    end

    对象.each do |行元素|
      结果 << 表头.map do |item|
        self.参数值处理(当前值: 行元素[item])
      end.join('|')
    end

    [true, 结果.join("\n")]
  end
end