module XuanRan
  def 表格_纵向(对象:, **args)
    结果 = []
    表头 = args[:表头] || ['<br>', '<br>']
    结果 << 表头.join('|')
    结果 << '-:|-'

    对象.each do |键, 值|
      结果 << [键, self.参数值处理(当前值: 值)].join('|')
    end

    [true, 结果.join("\n")]
  end
end