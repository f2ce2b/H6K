module XuanRan

  def 渲染表格(对象:,**args)
    result = case self.数据结构类型(对象: 对象)[0]
    when '[[字符串,数字]]', '{键:值}'
      self.表格_纵向(**args.merge!({对象: 对象}))
    when '[{键:值}]'
      self.表格_横向(**args.merge!({对象: 对象}))
    else
      断点调试(binding)
    end

    result
  end

  def 渲染(对象:, 渲染方式: nil, **args)    
    case 渲染方式
    when NilClass
      res = self.渲染表格(**args.merge!({对象: 对象}))
      return res unless args[:标题]
     
      标题 = args[:标题].map{|item| [ (args[:标题].index(item)+1).times.map{'#'}.join ,item].join(' ')}.join("\n\n")
      res[1] = [标题, res[1]].join("\n\n")

      res
    else
      当前路径 = File.expand_path(File.dirname(__FILE__))
      可用方法 = Dir["#{当前路径}/*/*.rb"].map{|file| [file.split('/')[-2], file.split('/')[-1].gsub('.rb', '')].join('_') }
      匹配方法 = 可用方法.select{|item| item.include?(渲染方式)}
      断点调试(binding) if 匹配方法.size != 1
      
      if self.respond_to?("#{匹配方法[0]}_校验")
        res = self.send("#{匹配方法[0]}_校验", **args.merge!({对象: 对象}))
        断点调试(binding) unless res[0]
      end

      断点调试(binding) unless self.respond_to?(匹配方法[0])
      self.send(匹配方法[0], **args.merge!({对象: 对象}))
    end
  end
end