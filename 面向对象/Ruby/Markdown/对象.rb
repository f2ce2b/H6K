class Markdown < Dobject
  def initialize(**params)
    current_path = File.expand_path(File.dirname(__FILE__))
    # params[:属性] = "#{current_path}/属性"
    params[:方法] = "#{current_path}/方法"
    super(**params)
  end

  def 参数值处理(当前值:)
    result = case 当前值
    when Array
      if 当前值.size == 1
        当前值[0]
      else
        当前值.map{|i| "〖#{当前值.index(i)+1}.〗#{i}"}.join('<br>')
      end
    when Hash
      参数值处理(当前值: 当前值.map{|key, value| [key, 参数值处理(当前值: value)].join(': ')})
    when /www\.|\.com|\.cn/
      "[#{当前值}](http://#{当前值})"
    else
      当前值
    end

    result
  end

end
