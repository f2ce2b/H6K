class ResultCache < Base

  def 缓存(盐: nil, 删除缓存: false, **args)
    md5 = Digest::MD5.hexdigest(args[:链接])
    file_name = [md5, 盐].delete_if{|item| item.nil?}.join('_')
    
    缓存文件路径 = File::join([self.目标目录, (args[:缓存文件夹]||''), ".#{file_name}.cache"])
    缓存文件夹 = 缓存文件路径.split('/')[0...-1].join('/')

    FileUtils.mkdir_p(缓存文件夹) unless File.directory?(缓存文件夹)

    File.delete(缓存文件路径) if 删除缓存 && File::file?(缓存文件路径)

    if File::file?(缓存文件路径)
      content = File::open(缓存文件路径){|文件对象|文件对象.read}
      formated = (JSON.parse(content) rescue content)
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 返回缓存: #{args[:链接]}\n"
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 缓存文件: #{缓存文件路径}\n"
      return [true, formated]
    elsif (过期缓存 = Dir["#{缓存文件夹}/.#{md5}*"]).size != 0
      过期缓存.map{|path| File.delete(path)}
    end

    args.merge!({请求模式: '直接请求'})
    result = F访问.请求(**args)
    断点调试(binding) unless result[0]

    File::open(缓存文件路径, 'w') do |文件对象|
      encoded = case result[1].to_s.encoding.to_s
      # 试验了几种转UTF-8的方法 只有Nokogiri的转换结果最好
      when 'ASCII-8BIT'
        Nokogiri::HTML.parse(result[1], "UTF-8").to_html rescue 断点调试(binding)
      when 'US-ASCII'
        if result[1] == []
          []
        else
          断点调试(binding)
        end
      when 'UTF-8'
        result[1]
      else
        断点调试(binding)
      end

      文件对象.write((encoded.to_json rescue encoded))
    end

    return result
  end

  def 缓存时效(时效:)
    return case 时效
    when '半年'
      str = []
      str << Time.now.year
      
      month = Time.now.month
      str << (month <= 6 ? 6 : 12)

      str << 30
      str.join
    else
      断点调试(binding)
    end
  end
end