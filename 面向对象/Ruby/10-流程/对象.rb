class Flow < Base

  def 跳出流程
    [false, 200]
  end

  def 重新循环流程
    return self.执行流程(流程: @流程, 对象: @对象)
  end

  def 进入断点调试
    断点调试(binding)
  end

  def 执行流程(流程:, 对象: )
    迭代子流程 = lambda do |子流程:, 键组:|
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 开始执行: #{键组.join(' -> ')}\n"
      if 对象.respond_to?(键组.last)
        res = 对象.send(键组.last)
        断点调试(binding) if res.nil?
        return res unless res[0] 
        (子流程||={}).delete_if{|k,v| ![res[1]].include?(k.to_s)}
      elsif self.respond_to?(键组.last)
        res = self.send(键组.last)
        断点调试(binding) if res.nil?
        return res unless res[0] 
        (子流程||={}).delete_if{|k,v| ![res[1]].include?(k.to_s)}
      elsif 子流程 == nil
        res = 对象.send(键组.last)
        断点调试(binding) if res.nil?
        断点调试(binding) unless res[0] 
        return res
      end

      case 子流程
      when Hash
        子流程.each do |键, 值|
          res = 迭代子流程.call(子流程: 值, 键组:[键组, 键].flatten)
          return res unless res[0]
        end
      when Array, String
        子流程 = [子流程].flatten
        子流程.each do |流程名|
          res = 迭代子流程.call(子流程: nil, 键组:[键组, 子流程[0...子流程.index(流程名)], 流程名].flatten)
          return res unless res[0]
        end
      else
        断点调试(binding)
      end
      res
    end
    
    @流程 = 流程
    @对象 = 对象
    @res = nil
    流程.each do |键, 值|
      @res = 迭代子流程.call(子流程: 值.clone, 键组:[键])
      return @res unless @res[0]
    end

    @res
  end
end