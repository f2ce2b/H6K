require_relative '../Ruby/Ruby.rb'

class Fund < StoreDir
  attr_accessor :结构路径

  def initialize(**params)
    super(**params)
    self.结构路径 ||= File.expand_path([File.dirname(__FILE__), './存储结构'].join('/'))
  end

  def 理想存储结构
    存储结构(目标目录: self.结构路径)
  end

end
