#!/bin/bash --login

function main_title() {
  json=$(jq '.' "$PWD/.$1.json")

  data_dir=$(jq -r '.["数据目录"]' <<<$json)
  steps=$(jq '.["步骤"]' <<<$json)

  keys=($(jq -c -r '.| keys_unsorted[]' <<<$steps))
  for key in "${keys[@]}";do
    detail=$(jq -r --arg key $key '.["\($key)"]' <<<$steps)
    [[ $(jq -r '.["是否运行"]' <<<$detail) == 'false' ]] && continue;

    params=$(jq -r '.["参数"]' <<<$detail)
    # 替换内容中有斜杠 只需改变分隔符即可
    params=$(echo $params | sed -e "s#数据目录#$data_dir#g")
    params=$(echo $params | sed -e "s/ //g")

    jq '.' <<<$params
    bash 55-运行.sh $params
    code=$?
    [[ $code -eq 0 ]] && continue;

    echo '[false, "'$1'中'$key'返回异常"]' 
    return $code
  done
}

main_title '股票运行流程'
code=$?; [[ $code -ne 0 ]] && exit $code;

echo '[true, ""]'