#!/bin/bash --login

# if [[ -n "\$BASH_SOURCE" ]];then return 255; else exit 255;fi

[ ! "$TOP" ] && export TOP="$(dirname $(readlink -f $0))";

target_path=$(jq -r '.["目标目录"]' <<< "$1")
if [ ! -d "$target_path" ] && echo '[false, "目标目录不存在", "'$target_path'"]';then 
  if [[ -n "\$BASH_SOURCE" ]]; then return 255; else exit 255;fi
fi

runtime_log="$target_path/.运行日志.log"

# 重定向至文件内时无法断点
# ruby "$TOP/面向流程/加载.rb" "$1"
# 重定向后触发代码里的断点后不会停止运行 这个对于并行化运行时存在一定风险
params=$(jq -c --arg flag true '.["并行模式"] = $flag' <<<$1)
ruby "$TOP/面向流程/加载.rb" "$params" >$runtime_log 2>&1

cat $runtime_log | tail -n 1
json=$(cat $runtime_log | tail -n 1)
jq '.' -e <<< "$json"
if [ ! $? -eq 0 ] && echo '[false, "返回结果格式错误"]' ;then
  if [[ -n "\$BASH_SOURCE" ]]; then return 255; else exit 255;fi
fi

if ([[ ! "$(jq '.[0]' <<<$json)" == 'true' ]]);then
  cat $runtime_log
  echo ''
  echo '[false, "运行异常"]'
  if [[ -n "\$BASH_SOURCE" ]]; then return 255; else exit 255;fi
fi

debug_file_path="$target_path/.调试模式.sh"
echo '#!/bin/bash --login' > "$debug_file_path"

function exec (){
  echo 'printf "[$(date '"'+%Y-%m-%d %H:%M:%S.%3N'"')] "' >> $debug_file_path
  suffix=${1##*.}
  if [[ "$suffix" == 'rb' ]]; then 
      echo "echo '开始执行: ${1}'" >> $debug_file_path
      echo "ruby '$1' '$2'" >> $debug_file_path
  else
    echo '[false, "尚未匹配 .'$suffix' 文件执行方式"]'
    if [[ -n "\$BASH_SOURCE" ]]; then return 255; else exit 255;fi
  fi
  
  echo 'code=$?; if [ ! $code -eq 0 ];then echo '"'[false, \"${1} 状态码异常\"]'"'; return $code; fi' >> $debug_file_path
  echo "rm $1" >> $debug_file_path
}

exec $(jq -r '.[1]' <<< "$json") "$1"

source "$debug_file_path"
code=$?

if [ $code -eq 0 ]; then
  rm "$debug_file_path"
  rm "$runtime_log"
else
  if [[ -n "\$BASH_SOURCE" ]]; then return 255; else exit 255;fi
fi

# bash 55-运行.sh '{"目标流程":"个股分析", "目标目录": "/home/ubuntu/nfs/data/股票/600519.SH"}'
# bash 55-运行.sh '{"目标流程":"个股分析", "目标目录": "/home/ubuntu/nfs/data/股票/000858.SZ"}'
# bash 55-运行.sh '{"目标流程":"股票数据缓存", "目标目录": "/home/ubuntu/nfs/data/股票/600519.SH"}'
# bash 55-运行.sh '{"目标流程":"数据统计", "目标目录": "/home/ubuntu/nfs/data"}'
# bash 55-运行.sh '{"目标流程":"目录索引", "目标目录": "/home/ubuntu/nfs/data"}'
# bash 55-运行.sh '{"目标流程":"个股基础关键词聚合", "目标目录": "/home/ubuntu/nfs/data/股票/000568.SZ"}'

# bash 55-运行.sh '{"目标流程":"个股行业分类", "目标目录": "/home/ubuntu/nfs/data/行业分类", "股票存储目录": "/home/ubuntu/nfs/data/股票"}'

# bash 55-运行.sh '{"目标流程":"个股行业分类", "目标目录": "/home/ubuntu/nfs/data/行业分类", "股票存储目录": "/home/ubuntu/nfs/data/股票"}'

# bash 55-运行.sh '{"目标流程":"个股行业分类", "目标目录": "/home/ubuntu/nfs/data/行业分类"}'

# bash 55-运行.sh '{"目标流程":"细分行业分析", "目标目录": "/home/ubuntu/nfs/data/行业分类/白酒"}'

# bash 55-运行.sh '{"目标流程":"股票关键词解析", "目标目录": "/home/ubuntu/nfs/data/关键词", "股票存储目录": "/home/ubuntu/nfs/data/股票"}'


# 集群相关
# bash 55-运行.sh '{"目标流程":"集群运算", "节点任务":"细分行业分析", "多线程模式": true, "目标目录": "/home/ubuntu/nfs/data/行业分类"}'
# bash 55-运行.sh '{"目标流程":"集群运算", "节点任务":"股票数据缓存", "多线程模式": true, "目标目录": "/home/ubuntu/nfs/data/股票"}'
# bash 55-运行.sh '{"目标流程":"集群运算", "节点任务":"个股分析", "多线程模式": true, "目标目录": "/home/ubuntu/nfs/data/股票"}'
# bash 55-运行.sh '{"目标流程":"集群运算", "节点任务":"个股基础关键词聚合", "多线程模式": true, "目标目录": "/home/ubuntu/nfs/data/股票"}'

# 基金相关
# bash 55-运行.sh '{"目标流程":"基金池更新", "目标目录": "/home/ubuntu/nfs/data/基金"}'
# 008975 161725 ETF不存在
# bash 55-运行.sh '{"目标目录": "/home/ubuntu/nfs/data/基金/159503"}'
# bash 55-运行.sh '{"目标目录": "/home/ubuntu/nfs/data/基金/159506"}'

# bash 55-运行.sh '{"目标流程":"基金数据缓存", "目标目录": "/home/ubuntu/nfs/data/基金/159503"}'
